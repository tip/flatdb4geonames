/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.util.Chronometer;

import fr.devinsy.strings.StringList;

/**
 * The Class IndexOfWordSeeks.
 */
public class IndexOfWordSeeks
{
	private static Logger logger = LoggerFactory.getLogger(IndexOfWordSeeks.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private RandomAccessFile data;
	private RandomAccessFile index1;
	private RandomAccessFile index2;
	private int index2LineLength;

	/**
	 * Instantiates a new index of word seeks.
	 *
	 * @param dataFile
	 *            the data file
	 * @param repository
	 *            the repository
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public IndexOfWordSeeks(final File dataFile, final File repository) throws IOException
	{
		if ((dataFile == null) || (repository == null))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.data = new RandomAccessFile(dataFile, "r");

			File index1File = new File(repository.getAbsoluteFile() + File.separator + IndexOfWordSeeksBuilder.INDEX1_FILENAME);
			this.index1 = new RandomAccessFile(index1File, "r");

			File index2File = new File(repository.getAbsoluteFile() + File.separator + IndexOfWordSeeksBuilder.INDEX2_FILENAME);
			this.index2 = new RandomAccessFile(index2File, "r");

			// Compute the line length of the index file for future seek
			// computations.
			this.index2.seek(0);
			this.index2LineLength = readUTFLineLength(this.index2) + 1;

			// Alternative method:
			// BufferedReader in = null;
			// try
			// {
			// in = new BufferedReader(new InputStreamReader(new
			// FileInputStream(index2File), DEFAULT_CHARSET_NAME));
			// this.index2LineLength = in.readLine().getBytes().length + 1;
			// logger.debug("index2.lineLength={}", this.index2LineLength);
			// }
			// finally
			// {
			// IOUtils.closeQuietly(in);
			// }
		}
	}

	/**
	 * Close.
	 */
	public void close()
	{
		IOUtils.closeQuietly(this.data);
		IOUtils.closeQuietly(this.index1);
		IOUtils.closeQuietly(this.index2);
	}

	/**
	 * Gets the.
	 *
	 * @param word
	 *            the word
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public StringList get(final String word) throws IOException
	{
		StringList result;

		result = new StringList();

		if (word != null)
		{
			Long seek = getSeek(word);

			if (seek != null)
			{
				this.index1.seek(seek);
				String indexLine = this.index1.readLine();

				for (String seekString : indexLine.split(","))
				{
					long dataSeek = Long.parseLong(seekString);
					this.data.seek(dataSeek);
					// String line = this.data.readLine();
					String line = readUTFLine(this.data);
					result.add(line);
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the index 2 line.
	 *
	 * @param lineIndex
	 *            the line index
	 * @return the index 2 line
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getIndex2Line(final long lineIndex) throws IOException
	{
		String result;

		this.index2.seek(lineIndex * this.index2LineLength);

		// We decrease lineLength from 1 to remove the EOL character.
		result = readUTFLine(this.index2, this.index2LineLength - 1);

		//
		return result;
	}

	/**
	 * Gets the seek.
	 *
	 * @param word
	 *            the word
	 * @return the seek
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Long getSeek(final String word) throws IOException
	{
		Long result;

		long min = 0;
		long max = index2LineCount() - 1;
		boolean ended = false;
		int stepCount = 0;
		Chronometer chrono = new Chronometer();
		result = null;
		while (!ended)
		{
			stepCount += 1;
			long dicho = (max + min) / 2; // min + ((max - min) /2)

			if (dicho < min)
			{
				ended = true;
				result = null;
			}
			else
			{
				String line = getIndex2Line(dicho);
				// logger.debug("dicho=" + dicho + "=>[" + line + "]");
				String candidate = line.split("\t")[0];

				logger.debug("{}\t{}\t{}\t{}\t[{}]", stepCount, min, max, dicho, candidate);

				if (word.compareTo(candidate) < 0)
				{
					max = dicho - 1;
				}
				else if (word.compareTo(candidate) > 0)
				{
					min = dicho + 1;
				}
				else
				{
					ended = true;
					result = Long.valueOf(line.split("\t")[1].trim());
				}
			}
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("result={}\ttime={}\tstepCount={}", result, Chronometer.toHumanString(chrono.stop().interval()), stepCount);
		}

		//
		return result;
	}

	/**
	 * Index 2 line count.
	 *
	 * @return the long
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private long index2LineCount() throws IOException
	{
		long result;

		result = this.index2.length() / this.index2LineLength;

		//
		return result;
	}

	/**
	 * RandomAccessFile methods do not manage UTF-8 as any field. So we need to
	 * implements a specific method to read UTF-8 line.
	 * 
	 * @param in
	 *            the in
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String readUTFLine(final RandomAccessFile in) throws IOException
	{
		String result;

		result = readUTFLine(in, readUTFLineLength(in));

		//
		return result;
	}

	/**
	 * Read UTF line.
	 *
	 * @param in
	 *            the in
	 * @param lineLength
	 *            the line length
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String readUTFLine(final RandomAccessFile in, final int lineLength) throws IOException
	{
		String result;

		byte[] buffer = new byte[lineLength];
		in.read(buffer);
		result = Charset.forName("utf-8").decode(ByteBuffer.wrap(buffer)).toString();

		//
		return result;
	}

	/**
	 * RandomAccessFile methods do not manage UTF-8 as any field. So we need to
	 * implements specific methods to read UTF-8 line.
	 * 
	 * This one computes the number of length of the line in the current
	 * position of the file, without the EOL characters.
	 * 
	 * @param in
	 *            the in
	 * @return the number of effective characters of the line without the EOL
	 *         character.
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static int readUTFLineLength(final RandomAccessFile in) throws IOException
	{
		int result;

		long currentSeek = in.getFilePointer();

		result = 0;
		boolean ended = false;
		while (!ended)
		{
			int value = in.read();

			switch (value)
			{
				case -1:
				case '\n':
				case '\r':
					ended = true;
				break;
				default:
					result += 1;
				break;
			}
		}

		in.seek(currentSeek);

		//
		return result;
	}
}
