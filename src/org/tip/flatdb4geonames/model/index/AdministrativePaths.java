/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AdministrativePaths.
 */
public class AdministrativePaths extends ArrayList<AdministrativePath>
{
	private static final long serialVersionUID = 3632860054662286444L;

	private static Logger logger = LoggerFactory.getLogger(AdministrativePaths.class);

	/**
	 * Instantiates a new administrative paths.
	 */
	public AdministrativePaths()
	{
		super();
	}

	/**
	 * Instantiates a new administrative paths.
	 *
	 * @param source
	 *            the source
	 */
	public AdministrativePaths(final AdministrativePaths source)
	{
		super(source.size());

		for (AdministrativePath code : source)
		{
			add(code);
		}
	}

	/**
	 * Instantiates a new administrative paths.
	 *
	 * @param capacity
	 *            the capacity
	 */
	public AdministrativePaths(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Gets the.
	 *
	 * @param tag
	 *            the tag
	 * @return the administrative path
	 */
	public AdministrativePath get(final String tag)
	{
		AdministrativePath result;

		if (StringUtils.isBlank(tag))
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			int index = 0;
			result = null;
			while (!ended)
			{
				if (index < this.size())
				{
					AdministrativePath code = this.get(index);
					if (code.getTag().equals(tag))
					{
						ended = true;
						result = code;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}
}
