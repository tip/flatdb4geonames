/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class BuildInformation.
 */
public class BuildInformation
{
	private static Logger logger = LoggerFactory.getLogger(BuildInformation.class);

	private static final String BUILD_INFORMATION_FILE = "/org/tip/flatdb4geonames/build_information.properties";

	private String productName;
	private String majorRevision;
	private String minorRevision;
	private String buildNumber;
	private String buildDate;
	private String generator;
	private String author;

	/**
	 * Instantiates a new builds the information.
	 */
	public BuildInformation()
	{
		Properties build = new Properties();

		try
		{
			//
			URL buildInformationFile = BuildInformation.class.getResource(BUILD_INFORMATION_FILE);

			if (buildInformationFile != null)
			{
				build.load(BuildInformation.class.getResource(BUILD_INFORMATION_FILE).openStream());
			}

			//
			this.productName = build.getProperty("product.name", "FlatDB4GeoNames");
			this.majorRevision = build.getProperty("product.revision.major", "d");
			this.minorRevision = build.getProperty("product.revision.minor", "e");
			this.buildNumber = build.getProperty("product.revision.build", "v");
			this.buildDate = build.getProperty("product.revision.date", "today");
			this.generator = build.getProperty("product.revision.generator", "n/a");
			this.author = build.getProperty("product.revision.author", "n/a");

		}
		catch (IOException exception)
		{
			//
			logger.error("Error loading the build.properties file: " + exception.getMessage());
			logger.error(ExceptionUtils.getStackTrace(exception));

			//
			this.productName = "n/a";
			this.majorRevision = "n/a";
			this.minorRevision = "n/a";
			this.buildNumber = "n/a";
			this.buildDate = "n/a";
			this.generator = "n/a";
			this.author = "n/a";
		}
	}

	/**
	 * Author.
	 * 
	 * @return the string
	 */
	public String author()
	{
		return this.author;
	}

	/**
	 * Builds the date.
	 * 
	 * @return the string
	 */
	public String buildDate()
	{
		return this.buildDate;
	}

	/**
	 * Builds the number.
	 * 
	 * @return the string
	 */
	public String buildNumber()
	{
		return this.buildNumber;
	}

	/**
	 * Generator.
	 * 
	 * @return the string
	 */
	public String generator()
	{
		return this.generator;
	}

	/**
	 * Major revision.
	 * 
	 * @return the string
	 */
	public String majorRevision()
	{
		return this.majorRevision;
	}

	/**
	 * Minor revision.
	 * 
	 * @return the string
	 */
	public String minorRevision()
	{
		return this.minorRevision;
	}

	/**
	 * Product name.
	 * 
	 * @return the string
	 */
	public String productName()
	{
		return this.productName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = String.format("%s %s.%s.%s built on %s by %s", this.productName, this.majorRevision, this.minorRevision, this.buildNumber, this.buildDate, this.author);

		//
		return result;
	}

	/**
	 * Version.
	 * 
	 * @return the string
	 */
	public String version()
	{
		String result;

		result = String.format("%s.%s.%s", this.majorRevision, this.minorRevision, this.buildNumber);

		//
		return result;
	}

	/**
	 * Checks if is defined.
	 * 
	 * @return true, if is defined
	 */
	public static boolean isDefined()
	{
		boolean result;

		if (BuildInformation.class.getResource(BUILD_INFORMATION_FILE) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}
}
