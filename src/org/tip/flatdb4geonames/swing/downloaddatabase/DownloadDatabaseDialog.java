/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.swing.downloaddatabase;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesFactory;
import org.tip.flatdb4geonames.util.Chronometer;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 * The Class DownloadDatabaseDialog.
 */
public class DownloadDatabaseDialog extends JDialog
{
	private static final long serialVersionUID = 9185519796738031776L;

	private static Logger logger = LoggerFactory.getLogger(DownloadDatabaseDialog.class);

	private final JPanel contentPanel = new JPanel();
	private JTextField txtfldTargetDirectory;
	private JTextField txtfldFreeDiskSpace;
	private JButton btnDownload;
	private JComboBox cmbxSourceRepository;
	private SwingWorker<Boolean, Integer> downloadWorker;
	private SwingWorker<Boolean, Integer> progressWorker;
	private JButton btnCancel;
	private JButton btnDone;
	private JProgressBar progressBarDatabaseZip;
	private JProgressBar progressBarDatabaseUnzip;
	private JLabel lblAllDatabaseZip;
	private JLabel lblAllDatabaseUnzip;
	private Chronometer chrono;
	private JLabel lblTimer;
	private JButton btnSelector;

	/**
	 * Instantiates a new download database dialog.
	 */
	public DownloadDatabaseDialog()
	{
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Download Datbase");
		setBounds(100, 100, 630, 420);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel introPanel = new JPanel();
			this.contentPanel.add(introPanel);
			{
				JLabel lblNewLabel = new JLabel(
						"<html>\nFlatDB4GeoNames provides quick and easy access to GeoNames data as local files.<br/>\nFlatDB4GeoNames requires GeoNames files and dedicated indexes.<br/>\nThis panel will download them for you in the directory of your choice.\n</html>");
				introPanel.add(lblNewLabel);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(10);
			this.contentPanel.add(verticalStrut);
		}
		{
			JPanel inputPanel = new JPanel();
			this.contentPanel.add(inputPanel);
			inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
					FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
					FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
					FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
					FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, }));
			{
				JLabel lblSource = new JLabel("Source repository:");
				inputPanel.add(lblSource, "2, 2, right, default");
			}
			{
				this.cmbxSourceRepository = new JComboBox();
				this.cmbxSourceRepository.setEditable(true);
				this.cmbxSourceRepository.setModel(new DefaultComboBoxModel(new String[] { "https://www.devinsy.fr/Puck/Repository/" }));
				inputPanel.add(this.cmbxSourceRepository, "4, 2, fill, default");
			}
			{
				JLabel lblTarget = new JLabel("Target directory:");
				inputPanel.add(lblTarget, "2, 4, right, default");
			}
			{
				this.txtfldTargetDirectory = new JTextField();
				this.txtfldTargetDirectory.setEditable(false);
				inputPanel.add(this.txtfldTargetDirectory, "4, 4, fill, default");
				this.txtfldTargetDirectory.setColumns(10);
			}
			{
				this.btnSelector = new JButton("...");
				this.btnSelector.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Select target directory button.
						File target = DownloadTargetSelector.showSelectorDialog(null, null);

						if (target != null)
						{
							setTargetDirectory(target);
						}
					}
				});
				inputPanel.add(this.btnSelector, "6, 4");
			}
			{
				JLabel lblSpaceDisk = new JLabel("Free disk space:");
				inputPanel.add(lblSpaceDisk, "2, 6, right, default");
			}
			{
				this.txtfldFreeDiskSpace = new JTextField();
				this.txtfldFreeDiskSpace.setEditable(false);
				this.txtfldFreeDiskSpace.setHorizontalAlignment(SwingConstants.RIGHT);
				inputPanel.add(this.txtfldFreeDiskSpace, "4, 6, fill, default");
				this.txtfldFreeDiskSpace.setColumns(10);
			}
			{
				JLabel lblfreeDiskSpace = new JLabel("Free disk space required:      3 000MB ");
				inputPanel.add(lblfreeDiskSpace, "4, 8, right, default");
			}
			{
				JPanel panel = new JPanel();
				inputPanel.add(panel, "4, 9, fill, fill");
				panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			}
			{
				this.lblAllDatabaseZip = new JLabel("database.zip:");
				this.lblAllDatabaseZip.setEnabled(false);
				inputPanel.add(this.lblAllDatabaseZip, "2, 12, right, default");
			}
			{
				this.progressBarDatabaseZip = new JProgressBar();
				this.progressBarDatabaseZip.setEnabled(false);
				inputPanel.add(this.progressBarDatabaseZip, "4, 12");
			}
			{
				this.lblAllDatabaseUnzip = new JLabel("unzip database.zip:");
				this.lblAllDatabaseUnzip.setEnabled(false);
				inputPanel.add(this.lblAllDatabaseUnzip, "2, 14, right, default");
			}
			{
				this.progressBarDatabaseUnzip = new JProgressBar();
				this.progressBarDatabaseUnzip.setEnabled(false);
				inputPanel.add(this.progressBarDatabaseUnzip, "4, 14");
			}
			{
				this.lblTimer = new JLabel("00:00:00");
				inputPanel.add(this.lblTimer, "4, 16, center, default");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				this.btnCancel = new JButton("Cancel");
				this.btnCancel.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Cancel button.

						//
						if (DownloadDatabaseDialog.this.downloadWorker != null)
						{
							DownloadDatabaseDialog.this.downloadWorker.cancel(true);
						}

						//
						if (DownloadDatabaseDialog.this.progressWorker != null)
						{
							DownloadDatabaseDialog.this.progressWorker.cancel(true);
						}

						//
						clearTarget(DownloadDatabaseDialog.this.txtfldTargetDirectory.getText());

						//
						dispose();
					}
				});
				this.btnCancel.setActionCommand("Cancel");
				buttonPane.add(this.btnCancel);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDownload = new JButton("Download");
				buttonPane.add(this.btnDownload);
				this.btnDownload.setEnabled(false);
				this.btnDownload.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Download button.
						final DownloadDatabaseCriteria criteria = getCriteria();

						// /////////////////////////////////////////////////////////
						try
						{
							updateFreeDiskSpace();

							DownloadDatabaseDialog.this.btnDownload.setEnabled(false);
							DownloadDatabaseDialog.this.btnSelector.setEnabled(false);
							DownloadDatabaseDialog.this.lblAllDatabaseZip.setEnabled(true);
							DownloadDatabaseDialog.this.lblAllDatabaseUnzip.setEnabled(true);
							DownloadDatabaseDialog.this.progressBarDatabaseZip.setEnabled(true);
							DownloadDatabaseDialog.this.progressBarDatabaseUnzip.setEnabled(true);

							DownloadDatabaseDialog.this.chrono = new Chronometer();

							//
							DownloadDatabaseDialog.this.downloadWorker = new SwingWorker<Boolean, Integer>()
							{
								@Override
								protected Boolean doInBackground()
								{
									Boolean result;

									try
									{
										clearTarget(criteria.getTargetDirectory());
										File targetFile = new File(criteria.getTargetDirectory());
										FlatDB4GeoNames.downloadDatabase(criteria.getSourceRepository(), targetFile);

										result = true;
									}
									catch (FileNotFoundException exception)
									{
										exception.printStackTrace();

										String title = "Error";
										String message = "File not found: " + exception.getMessage();

										JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

										result = false;
									}
									catch (Exception exception)
									{
										exception.printStackTrace();

										String title = "Error";
										String message = "Download error: " + exception.getMessage();

										JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

										result = false;
									}

									//
									return result;
								}

								// Can safely update the GUI from this
								// method.
								@Override
								protected void done()
								{
									// Retrieve the return value of
									// doInBackground.
									logger.debug("download progress done");

									DownloadDatabaseDialog.this.btnCancel.setEnabled(false);
									DownloadDatabaseDialog.this.btnDownload.setEnabled(false);
									DownloadDatabaseDialog.this.btnDone.setEnabled(true);
									if (DownloadDatabaseDialog.this.progressWorker != null)
									{
										DownloadDatabaseDialog.this.progressWorker.cancel(true);
									}
									DownloadDatabaseDialog.this.progressBarDatabaseZip.setValue(100);
									DownloadDatabaseDialog.this.progressBarDatabaseUnzip.setValue(100);
								}
							};
							DownloadDatabaseDialog.this.downloadWorker.execute();

							//
							final long databaseZipLength;

							if (criteria.getSourceRepository().startsWith("http"))
							{
								URL databaseZipURL = new URL(criteria.getSourceRepository() + FlatDB4GeoNamesFactory.DEFAULT_DATABASE_FILE);
								databaseZipLength = databaseZipURL.openConnection().getContentLength();
							}
							else
							{
								databaseZipLength = new File(criteria.getSourceRepository() + FlatDB4GeoNamesFactory.DEFAULT_DATABASE_FILE).length();
							}

							logger.debug("databaseZipLength=" + databaseZipLength);

							DownloadDatabaseDialog.this.progressWorker = new SwingWorker<Boolean, Integer>()
							{
								@Override
								protected Boolean doInBackground() throws InterruptedException
								{
									boolean ended = false;
									while (!ended)
									{
										Thread.sleep(1000);
										publish();
									}

									return true;
								}

								// Can safely update the GUI from this
								// method.
								@Override
								protected void done()
								{
									logger.debug("progressworker done");
								}

								@Override
								// Can safely update the GUI from this
								// method.
								protected void process(final List<Integer> chunks)
								{
									logger.debug("progress worker process");

									//
									updateFreeDiskSpace();

									//
									File targetFile = new File(criteria.getTargetDirectory() + File.separator + FlatDB4GeoNamesFactory.DEFAULT_DATABASE_FILE);
									int percentage;
									if (targetFile.exists())
									{
										percentage = (int) (targetFile.length() * 100 / databaseZipLength);
									}
									else
									{
										percentage = 0;
									}
									DownloadDatabaseDialog.this.progressBarDatabaseZip.setValue(percentage);
									DownloadDatabaseDialog.this.progressBarDatabaseUnzip.setValue(0);

									//
									String timerValue = Chronometer.toTimer(DownloadDatabaseDialog.this.chrono.stop().interval());
									DownloadDatabaseDialog.this.lblTimer.setText(timerValue);
								}
							};

							DownloadDatabaseDialog.this.progressWorker.execute();
						}
						catch (final Exception exception)
						{
							//
							String title = "Error computerum est";
							String message = "Error occured during working: " + exception.getMessage();

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				this.btnDownload.setActionCommand("OK");
				getRootPane().setDefaultButton(this.btnDownload);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDone = new JButton("Done");
				this.btnDone.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Button done.
						dispose();
					}
				});
				this.btnDone.setEnabled(false);
				this.btnDone.setActionCommand("OK");
				buttonPane.add(this.btnDone);
			}
		}
	}

	/**
	 * Clear target.
	 * 
	 * @param path
	 *            the path
	 */
	public void clearTarget(final String path)
	{
		if (StringUtils.isNotBlank(path))
		{
			new File(path, "allCountries.zip").delete();
			new File(path, "allCountries.txt").delete();
			new File(path, "featureCodes_en.txt").delete();
			new File(path, "admin1CodesASCII.txt").delete();
			new File(path, "admin2Codes.txt").delete();
			new File(path, "README").delete();
		}
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	private DownloadDatabaseCriteria getCriteria()
	{
		DownloadDatabaseCriteria result;

		result = new DownloadDatabaseCriteria();

		result.setSourceRepository((String) this.cmbxSourceRepository.getSelectedItem());
		result.setTargetDirectory(this.txtfldTargetDirectory.getText());

		//
		return result;
	}

	/**
	 * Sets the target directory.
	 * 
	 * @param target
	 *            the new target directory
	 */
	public void setTargetDirectory(final File target)
	{
		if ((target != null) && (target.exists()) && (target.isDirectory()))
		{
			if (target.canWrite())
			{
				DownloadDatabaseDialog.this.txtfldTargetDirectory.setText(target.getAbsolutePath());

				updateFreeDiskSpace();

				this.btnDownload.setEnabled(true);
			}
			else
			{
				String title = "Warning";
				String message = "This directory is not writable.";

				JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Update free disk space.
	 */
	public void updateFreeDiskSpace()
	{
		String value = String.format("%,dMB", new File(this.txtfldTargetDirectory.getText()).getUsableSpace() / 1024 / 1024);
		this.txtfldFreeDiskSpace.setText(value);
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		try
		{
			new DownloadDatabaseDialog();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	/**
	 * This method shows dialog centered with screen.
	 */
	public static void showDialog()
	{
		showDialog(null);
	}

	/**
	 * This method shows dialog.
	 * 
	 * @param parent
	 *            the parent
	 */
	public static void showDialog(final Component parent)
	{
		//
		DownloadDatabaseDialog dialog = new DownloadDatabaseDialog();
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
