/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views.downloadgeonamesfiles;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesFactory;
import org.tip.flatdb4geonames.util.Chronometer;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

/**
 * The Class DownloadGeoNamesFilesDialog.
 */
public class DownloadGeoNamesFilesDialog extends JDialog
{
	private static final long serialVersionUID = -9132841607108696442L;

	private static Logger logger = LoggerFactory.getLogger(DownloadGeoNamesFilesDialog.class);

	private final JPanel contentPanel = new JPanel();
	private JTextField txtfldTargetDirectory;
	private JTextField txtfldFreeDiskSpace;
	private JButton btnDownload;
	private JComboBox cmbxSourceRepository;
	private SwingWorker<Boolean, Integer> downloadWorker;
	private SwingWorker<Boolean, Integer> progressWorker;
	private JButton btnCancel;
	private JButton btnDone;
	private JProgressBar progressBarAllCountriesZip;
	private JProgressBar progressBarAllCountriesTxt;
	private JProgressBar progressBarFeatureCodesEnTxt;
	private JLabel lblAllCountriesZip;
	private JLabel lblAllCountriesTxt;
	private JLabel lblFeatureCodeEnTxt;
	private Chronometer chrono;
	private JLabel lblTimer;
	private JButton btnSelector;
	private JLabel lblAdmin1Codes;
	private JLabel lblAdmin2Codes;
	private JProgressBar progressBarAdmin1Codes;
	private JProgressBar progressBarAdmin2Codes;

	/**
	 * Instantiates a new download GeoNames files dialog.
	 */
	public DownloadGeoNamesFilesDialog()
	{
		setModal(true);
		setTitle("Download GeoNames dump files");
		setBounds(100, 100, 630, 450);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel introPanel = new JPanel();
			this.contentPanel.add(introPanel);
			{
				JLabel lblNewLabel = new JLabel(
						"<html>\nGeoNames provides official dump files.<br/>\nFlatDB4GeoNames requires some of them.<br/>\nThis panel will download them for you in the directory of your choice.\n</html>");
				introPanel.add(lblNewLabel);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(10);
			this.contentPanel.add(verticalStrut);
		}
		{
			JPanel inputPanel = new JPanel();
			this.contentPanel.add(inputPanel);
			inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
					FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
					FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
					FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
					FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
					FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
					FormSpecs.DEFAULT_ROWSPEC, }));
			{
				JLabel lblSource = new JLabel("Source repository:");
				inputPanel.add(lblSource, "2, 2, right, default");
			}
			{
				this.cmbxSourceRepository = new JComboBox();
				this.cmbxSourceRepository.setModel(new DefaultComboBoxModel(new String[] { "http://download.geonames.org/export/dump/" }));
				inputPanel.add(this.cmbxSourceRepository, "4, 2, fill, default");
			}
			{
				JLabel lblTarget = new JLabel("Target directory:");
				inputPanel.add(lblTarget, "2, 4, right, default");
			}
			{
				this.txtfldTargetDirectory = new JTextField();
				this.txtfldTargetDirectory.setEditable(false);
				inputPanel.add(this.txtfldTargetDirectory, "4, 4, fill, default");
				this.txtfldTargetDirectory.setColumns(10);
			}
			{
				this.btnSelector = new JButton("...");
				this.btnSelector.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Select target directory button.
						File target = DownloadTargetSelector.showSelectorDialog(null, null);

						if (target != null)
						{
							setTargetDirectory(target);
						}
					}
				});
				inputPanel.add(this.btnSelector, "6, 4");
			}
			{
				JLabel lblSpaceDisk = new JLabel("Free disk space:");
				inputPanel.add(lblSpaceDisk, "2, 6, right, default");
			}
			{
				this.txtfldFreeDiskSpace = new JTextField();
				this.txtfldFreeDiskSpace.setEditable(false);
				this.txtfldFreeDiskSpace.setHorizontalAlignment(SwingConstants.RIGHT);
				inputPanel.add(this.txtfldFreeDiskSpace, "4, 6, fill, default");
				this.txtfldFreeDiskSpace.setColumns(10);
			}
			{
				this.lblAllCountriesZip = new JLabel("allCountries.zip:");
				this.lblAllCountriesZip.setEnabled(false);
				inputPanel.add(this.lblAllCountriesZip, "2, 12, right, default");
			}
			{
				this.progressBarAllCountriesZip = new JProgressBar();
				this.progressBarAllCountriesZip.setEnabled(false);
				inputPanel.add(this.progressBarAllCountriesZip, "4, 12");
			}
			{
				this.lblAllCountriesTxt = new JLabel("unzip allCountries.zip:");
				this.lblAllCountriesTxt.setEnabled(false);
				inputPanel.add(this.lblAllCountriesTxt, "2, 14, right, default");
			}
			{
				this.progressBarAllCountriesTxt = new JProgressBar();
				this.progressBarAllCountriesTxt.setEnabled(false);
				inputPanel.add(this.progressBarAllCountriesTxt, "4, 14");
			}
			{
				this.lblFeatureCodeEnTxt = new JLabel("featureCodes_en.txt:");
				this.lblFeatureCodeEnTxt.setEnabled(false);
				inputPanel.add(this.lblFeatureCodeEnTxt, "2, 16, right, default");
			}
			{
				this.progressBarFeatureCodesEnTxt = new JProgressBar();
				this.progressBarFeatureCodesEnTxt.setEnabled(false);
				inputPanel.add(this.progressBarFeatureCodesEnTxt, "4, 16");
			}
			{
				this.lblAdmin1Codes = new JLabel("admin1CodesASCII.txt:");
				this.lblAdmin1Codes.setEnabled(false);
				inputPanel.add(this.lblAdmin1Codes, "2, 18, right, default");
			}
			{
				this.progressBarAdmin1Codes = new JProgressBar();
				this.progressBarAdmin1Codes.setEnabled(false);
				inputPanel.add(this.progressBarAdmin1Codes, "4, 18");
			}
			{
				this.lblAdmin2Codes = new JLabel("admin2Codes.txt:");
				this.lblAdmin2Codes.setEnabled(false);
				inputPanel.add(this.lblAdmin2Codes, "2, 20, right, default");
			}
			{
				this.progressBarAdmin2Codes = new JProgressBar();
				this.progressBarAdmin2Codes.setEnabled(false);
				inputPanel.add(this.progressBarAdmin2Codes, "4, 20");
			}
			{
				this.lblTimer = new JLabel("00:00:00");
				inputPanel.add(this.lblTimer, "4, 23, center, default");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				this.btnCancel = new JButton("Cancel");
				this.btnCancel.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Cancel button.

						//
						if (DownloadGeoNamesFilesDialog.this.downloadWorker != null)
						{
							DownloadGeoNamesFilesDialog.this.downloadWorker.cancel(true);
						}

						//
						if (DownloadGeoNamesFilesDialog.this.progressWorker != null)
						{
							DownloadGeoNamesFilesDialog.this.progressWorker.cancel(true);
						}

						//
						clearTarget(DownloadGeoNamesFilesDialog.this.txtfldTargetDirectory.getText());

						//
						dispose();
					}
				});
				this.btnCancel.setActionCommand("Cancel");
				buttonPane.add(this.btnCancel);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDownload = new JButton("Download");
				buttonPane.add(this.btnDownload);
				this.btnDownload.setEnabled(false);
				this.btnDownload.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Download button.
						final DownloadGeoNamesFilesCriteria criteria = getCriteria();

						// /////////////////////////////////////////////////////////
						try
						{
							// criteria.setSourceRepository("http://www.devinsy.fr/Public/GeoTest/");

							updateFreeDiskSpace();

							DownloadGeoNamesFilesDialog.this.btnDownload.setEnabled(false);
							DownloadGeoNamesFilesDialog.this.btnSelector.setEnabled(false);
							DownloadGeoNamesFilesDialog.this.lblAllCountriesZip.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.lblAllCountriesTxt.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.lblFeatureCodeEnTxt.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.lblAdmin1Codes.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.lblAdmin2Codes.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.progressBarAllCountriesZip.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.progressBarAllCountriesTxt.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.progressBarFeatureCodesEnTxt.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.progressBarAdmin1Codes.setEnabled(true);
							DownloadGeoNamesFilesDialog.this.progressBarAdmin2Codes.setEnabled(true);

							DownloadGeoNamesFilesDialog.this.chrono = new Chronometer();

							//
							DownloadGeoNamesFilesDialog.this.downloadWorker = new SwingWorker<Boolean, Integer>()
							{
								@Override
								protected Boolean doInBackground()
								{
									Boolean result;

									try
									{
										clearTarget(criteria.getTargetDirectory());
										File targetFile = new File(criteria.getTargetDirectory());
										FlatDB4GeoNamesFactory.downloadGeoNamesFiles(criteria.getSourceRepository(), targetFile);

										result = true;
									}
									catch (FileNotFoundException exception)
									{
										exception.printStackTrace();

										String title = "Error";
										String message = "File not found: " + exception.getMessage();

										JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

										result = false;
									}
									catch (Exception exception)
									{
										exception.printStackTrace();

										String title = "Error";
										String message = "Download error: " + exception.getMessage();

										JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

										result = false;
									}

									//
									return result;
								}

								// Can safely update the GUI from this
								// method.
								@Override
								protected void done()
								{
									// Retrieve the return value of
									// doInBackground.
									logger.debug("download progress done");

									DownloadGeoNamesFilesDialog.this.btnCancel.setEnabled(false);
									DownloadGeoNamesFilesDialog.this.btnDownload.setEnabled(false);
									DownloadGeoNamesFilesDialog.this.btnDone.setEnabled(true);
									DownloadGeoNamesFilesDialog.this.progressWorker.cancel(true);
									DownloadGeoNamesFilesDialog.this.progressBarAllCountriesZip.setValue(100);
									DownloadGeoNamesFilesDialog.this.progressBarAllCountriesTxt.setValue(100);
									DownloadGeoNamesFilesDialog.this.progressBarFeatureCodesEnTxt.setValue(100);
									DownloadGeoNamesFilesDialog.this.progressBarAdmin1Codes.setValue(100);
									DownloadGeoNamesFilesDialog.this.progressBarAdmin2Codes.setValue(100);
								}
							};
							DownloadGeoNamesFilesDialog.this.downloadWorker.execute();

							//
							URL allCountriesZipURL = new URL(criteria.getSourceRepository() + "/allCountries.zip");
							final long allCountriesZipLength = allCountriesZipURL.openConnection().getContentLength();

							logger.debug("allCountriesZipLength=" + allCountriesZipLength);

							DownloadGeoNamesFilesDialog.this.progressWorker = new SwingWorker<Boolean, Integer>()
							{
								@Override
								protected Boolean doInBackground() throws InterruptedException
								{
									boolean ended = false;
									while (!ended)
									{
										Thread.sleep(1000);
										publish();
									}

									return true;
								}

								// Can safely update the GUI from this
								// method.
								@Override
								protected void done()
								{
									logger.debug("progressworker done");
								}

								@Override
								// Can safely update the GUI from this
								// method.
								protected void process(final List<Integer> chunks)
								{
									logger.debug("progress worker process");

									//
									updateFreeDiskSpace();

									//
									if (new File(criteria.getTargetDirectory() + "/" + "allCountries.txt").exists())
									{
										DownloadGeoNamesFilesDialog.this.progressBarAllCountriesZip.setValue(100);
										DownloadGeoNamesFilesDialog.this.progressBarAllCountriesTxt.setValue(100);
									}
									else
									{
										int percentage;
										File targetAllCountriesZipFile = new File(criteria.getTargetDirectory() + "/" + "allCountries.zip");
										if (targetAllCountriesZipFile.exists())
										{
											percentage = (int) (targetAllCountriesZipFile.length() * 100 / allCountriesZipLength);
										}
										else
										{
											percentage = 0;
										}
										DownloadGeoNamesFilesDialog.this.progressBarAllCountriesZip.setValue(percentage);

										DownloadGeoNamesFilesDialog.this.progressBarAllCountriesTxt.setValue(0);
									}

									//
									int percentage;
									if (new File(criteria.getTargetDirectory() + "/" + "featureCodes_en.txt").exists())
									{
										percentage = 100;
									}
									else
									{
										percentage = 0;
									}
									DownloadGeoNamesFilesDialog.this.progressBarFeatureCodesEnTxt.setValue(percentage);

									//
									String timerValue = Chronometer.toTimer(DownloadGeoNamesFilesDialog.this.chrono.stop().interval());
									DownloadGeoNamesFilesDialog.this.lblTimer.setText(timerValue);
								}
							};

							DownloadGeoNamesFilesDialog.this.progressWorker.execute();
						}
						catch (final IOException exception)
						{
							//
							String title = "Error computerum est";
							String message = "Error occured during working: " + exception.getMessage();

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				this.btnDownload.setActionCommand("OK");
				getRootPane().setDefaultButton(this.btnDownload);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDone = new JButton("Done");
				this.btnDone.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(final ActionEvent event)
					{
						// Button done.
						dispose();
					}
				});
				this.btnDone.setEnabled(false);
				this.btnDone.setActionCommand("OK");
				buttonPane.add(this.btnDone);
			}
		}
	}

	/**
	 * Clear target.
	 * 
	 * @param path
	 *            the path
	 */
	public void clearTarget(final String path)
	{
		if (StringUtils.isNotBlank(path))
		{
			new File(path, "allCountries.zip").delete();
			new File(path, "allCountries.txt").delete();
			new File(path, "featureCodes_en.txt").delete();
			new File(path, "admin1CodesASCII.txt").delete();
			new File(path, "admin2Codes.txt").delete();
			new File(path, "README").delete();
			new File(path, "id_seek.index").delete();
			new File(path, "seeks.index").delete();
			new File(path, "word_seek.index").delete();
			new File(path, "word_seeks.index.rejection").delete();
		}
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	private DownloadGeoNamesFilesCriteria getCriteria()
	{
		DownloadGeoNamesFilesCriteria result;

		result = new DownloadGeoNamesFilesCriteria();

		result.setSourceRepository((String) this.cmbxSourceRepository.getSelectedItem());
		result.setTargetDirectory(this.txtfldTargetDirectory.getText());

		//
		return result;
	}

	/**
	 * Sets the target directory.
	 * 
	 * @param target
	 *            the new target directory
	 */
	public void setTargetDirectory(final File target)
	{
		if ((target != null) && (target.exists()) && (target.isDirectory()))
		{
			if (target.canWrite())
			{
				DownloadGeoNamesFilesDialog.this.txtfldTargetDirectory.setText(target.getAbsolutePath());

				updateFreeDiskSpace();

				this.btnDownload.setEnabled(true);
			}
			else
			{
				String title = "Warning";
				String message = "This directory is not writable.";

				JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Update free disk space.
	 */
	public void updateFreeDiskSpace()
	{
		String value = String.format("%,dMB", new File(this.txtfldTargetDirectory.getText()).getUsableSpace() / 1024 / 1024);
		this.txtfldFreeDiskSpace.setText(value);
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		try
		{
			new DownloadGeoNamesFilesDialog();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	/**
	 * This method shows the dialog in center of the screen.
	 */
	public static void showDialog()
	{
		showDialog(null);
	}

	/**
	 * This method shows the dialog.
	 * 
	 * @param parent
	 *            the parent
	 */
	public static void showDialog(final Component parent)
	{
		//
		DownloadGeoNamesFilesDialog dialog = new DownloadGeoNamesFilesDialog();
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
