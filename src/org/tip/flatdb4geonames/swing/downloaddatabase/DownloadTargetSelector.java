/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.swing.downloaddatabase;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DownloadTargetSelector.
 */
public class DownloadTargetSelector extends JFileChooser
{
	private static final long serialVersionUID = -1252036852424498268L;

	private static Logger logger = LoggerFactory.getLogger(DownloadTargetSelector.class);

	/**
	 * Instantiates a new download target selector.
	 * 
	 * @param targetFile
	 *            the target file
	 */
	public DownloadTargetSelector(final File targetFile)
	{
		super();

		//
		File file;
		if ((targetFile == null) || (StringUtils.isBlank(targetFile.getAbsolutePath())))
		{
			file = null;
		}
		else if (targetFile.isFile())
		{
			file = targetFile.getParentFile();
		}
		else
		{
			file = targetFile;
		}

		//
		setSelectedFile(file);
		setDialogTitle("Download directory");
		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Select");
		setDialogType(CUSTOM_DIALOG);
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#approveSelection()
	 */
	@Override
	public void approveSelection()
	{
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		super.approveSelection();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#cancelSelection()
	 */
	@Override
	public void cancelSelection()
	{
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#setSelectedFile(java.io.File)
	 */
	@Override
	public void setSelectedFile(final File file)
	{
		super.setSelectedFile(file);

		logger.info("SELECED FILE {}", file);
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#setVisible(boolean)
	 */
	@Override
	public void setVisible(final boolean visible)
	{
		//
		super.setVisible(visible);

		if (!visible)
		{
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 *            the parent
	 * @param targetFile
	 *            the target file
	 * @return the file
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile)
	{
		File result;

		//
		DownloadTargetSelector selector = new DownloadTargetSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION)
		{
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();
		}
		else
		{
			result = null;
		}

		//
		return result;
	}
}
