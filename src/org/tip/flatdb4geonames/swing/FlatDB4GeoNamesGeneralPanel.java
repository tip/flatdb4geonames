/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.tip.flatdb4geonames.gui.views.help.TutorialDialog;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringsUtils;

/**
 * The Class FlatDB4GeoNamesGeneralPanel.
 */
public class FlatDB4GeoNamesGeneralPanel extends JPanel
{
	private static final long serialVersionUID = 8900658076835485258L;
	private JEditorPane editorPane;

	/**
	 * Instantiates a new FlatDB4GeoNames general panel.
	 */
	public FlatDB4GeoNamesGeneralPanel()
	{
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		this.editorPane = new JEditorPane();
		editorPane.setBackground(new Color(238, 238, 238));
		this.editorPane.setEditable(false);
		this.editorPane.setContentType("text/html");
		scrollPane.setViewportView(this.editorPane);
	}

	/**
	 * Refresh.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void refresh() throws IOException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		if (database == null)
		{
			this.editorPane.setText("");
		}
		else
		{
			StringList lines = new StringList();

			lines.appendln("<p>Repository directory: ").append(database.getRepository().getAbsolutePath()).appendln("</p>");

			lines.appendln("<p>");
			lines.appendln("<table>");
			long total = 0;
			String[] filenames = database.getRepository().list();
			Arrays.sort(filenames);
			for (String filename : filenames)
			{
				File file = new File(database.getRepository() + File.separator + filename);

				lines.appendln("  <tr>");
				lines.append("    <td>").append(file.getName()).appendln("</td>");
				lines.append("    <td style=\"text-align: right;\">").append(String.format("%,d", file.length() / 1024)).append(" KB").appendln("</td>");
				lines.appendln("  </tr>");

				total += file.length();
			}
			lines.append("    <td style=\"text-align: right;\">").append("Total:").appendln("</td>");
			lines.append("    <td style=\"text-align: right;\">").append(String.format("%,d", total / 1024)).append(" KB").appendln("</td>");

			lines.appendln("</table>");
			lines.appendln("</p>");

			//
			StringList page = StringsUtils.load(TutorialDialog.class.getResource("/org/tip/flatdb4geonames/swing/default.html"));
			page.add(page.size() - 2, lines.toString());
			this.editorPane.setText(page.toString());
			this.editorPane.setCaretPosition(0);
		}
	}
}
