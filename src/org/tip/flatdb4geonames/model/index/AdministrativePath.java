/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;

/**
 * This class implements an administrative path.
 * 
 * An administrative code path is a concatenation of administrative codes
 * separated by point: CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
 * 
 * Examples of administrative path:
 * <ul>
 * <li>US: United-States</li>
 * <li>US.KY: Kentucky</li>
 * <li>US.KY.017: Bourbon County</li>
 * </ul>
 * 
 */
public class AdministrativePath
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AdministrativePath.class);

	private String codePath;
	private String toponym;
	private Integer geonameId;

	/**
	 * Instantiates a new administrative path.
	 *
	 * @param path
	 *            the path
	 * @param description
	 *            the description
	 * @param geonameId
	 *            the geoname id
	 */
	public AdministrativePath(final String path, final String description, final Integer geonameId)
	{
		if (StringUtils.isBlank(path))
		{
			throw new IllegalArgumentException("Null parameter.");
		}
		else
		{
			this.codePath = path;
			this.toponym = description;
			this.geonameId = geonameId;
		}
	}

	/**
	 * Gets the code 1.
	 *
	 * @return the code 1
	 */
	public String getCode1()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 1)
		{
			result = tokens[1];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Gets the code 2.
	 *
	 * @return the code 2
	 */
	public String getCode2()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 2)
		{
			result = tokens[2];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Gets the code 3.
	 *
	 * @return the code 3
	 */
	public String getCode3()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 3)
		{
			result = tokens[3];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Gets the code 4.
	 *
	 * @return the code 4
	 */
	public String getCode4()
	{
		String result;

		String[] tokens = this.codePath.split("\\.");

		if (tokens.length > 4)
		{
			result = tokens[4];
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Gets the county code.
	 *
	 * @return the county code
	 */
	public String getCountyCode()
	{
		String result;

		result = this.codePath.split("\\.")[0];

		//
		return result;
	}

	/**
	 * Gets the geoname id.
	 *
	 * @return the geoname id
	 */
	public int getGeonameId()
	{
		return this.geonameId;
	}

	/**
	 * Gets the tag.
	 *
	 * @return the tag
	 */
	public String getTag()
	{
		return this.codePath;
	}

	/**
	 * Gets the toponym.
	 *
	 * @return the toponym
	 */
	public String getToponym()
	{
		return this.toponym;
	}

	/**
	 * Sets the geoname id.
	 *
	 * @param geonameId
	 *            the new geoname id
	 */
	public void setGeonameId(final int geonameId)
	{
		this.geonameId = geonameId;
	}

	/**
	 * Sets the tag.
	 *
	 * @param tag
	 *            the new tag
	 */
	public void setTag(final String tag)
	{
		this.codePath = tag;
	}

	/**
	 * Sets the toponym.
	 *
	 * @param toponymValue
	 *            the new toponym
	 */
	public void setToponym(final String toponymValue)
	{
		this.toponym = toponymValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(this.codePath);
		buffer.append(this.geonameId);

		result = buffer.toStringSeparatedBy("\t");

		//
		return result;
	}

	/**
	 * Convert to code path.
	 *
	 * @param countryCode
	 *            the country code
	 * @param code1
	 *            the code 1
	 * @param code2
	 *            the code 2
	 * @param code3
	 *            the code 3
	 * @param code4
	 *            the code 4
	 * @return the string
	 */
	public static String convertToCodePath(final String countryCode, final String code1, final String code2, final String code3, final String code4)
	{
		String result;

		if (countryCode == null)
		{
			result = null;
		}
		else
		{
			StringList buffer = new StringList();
			buffer.append(countryCode);
			buffer.append(code1);
			buffer.append(code2);
			buffer.append(code3);
			buffer.append(code4);

			result = buffer.toStringSeparatedBy(".");
		}

		//
		return result;
	}
}
