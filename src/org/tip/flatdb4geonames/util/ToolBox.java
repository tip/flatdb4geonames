/*
 * Copyright (C) 2016-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ToolBox.
 */
public class ToolBox
{
	private static Logger logger = LoggerFactory.getLogger(ToolBox.class);

	/**
	 * Instantiates a new tool box.
	 */
	private ToolBox()
	{
	}

	/**
	 * Match any.
	 * 
	 * @param seq
	 *            the seq
	 * @param searchSeqs
	 *            the search seqs
	 * @return true, if successful
	 */
	public static boolean matchAny(final CharSequence seq, final CharSequence... searchSeqs)
	{
		boolean result;

		boolean ended = false;
		int index = 0;
		result = false;
		while (!ended)
		{
			if (index < searchSeqs.length)
			{
				CharSequence searchSeq = searchSeqs[index];
				if (StringUtils.equals(seq, searchSeq))
				{
					ended = true;
					result = true;
				}
				else
				{
					index += 1;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Normalize path.
	 * 
	 * @param sourcePath
	 *            the source path
	 * @return the file
	 */
	public static File normalizePath(final String sourcePath)
	{
		File result;

		if (StringUtils.isBlank(sourcePath))
		{
			result = null;
		}
		else
		{
			String targetPath;
			if (sourcePath.startsWith("~"))
			{
				targetPath = sourcePath.replaceFirst("~", System.getProperty("user.home"));
			}
			else
			{
				targetPath = sourcePath;
			}

			result = new File(targetPath);
		}

		//
		return result;
	}
}
