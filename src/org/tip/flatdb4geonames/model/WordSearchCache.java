/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class WordSearchCache.
 */
public class WordSearchCache
{
	private int capacity;
	private Map<String, GeoNamesLine> data;

	/**
	 * Instantiates a new word search cache.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public WordSearchCache(final int capacity)
	{
		this.capacity = capacity;
		this.data = new HashMap<String, GeoNamesLine>(capacity);
	}

	/**
	 * Put.
	 * 
	 * @param word
	 *            the word
	 * @param line
	 *            the line
	 */
	public void put(final String word, final GeoNamesLine line)
	{
		if (line != null)
		{
			this.data.put(word, line);

			if (this.data.size() == this.capacity)
			{
				// clean(); TODO
			}
		}
	}
}
