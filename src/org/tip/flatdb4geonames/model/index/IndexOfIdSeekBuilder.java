/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.util.Chronometer;
import org.tip.flatdb4geonames.util.StringFileSorter;

/**
 * The Class IndexOfIdSeekBuilder.
 */
public class IndexOfIdSeekBuilder
{
	private static Logger logger = LoggerFactory.getLogger(IndexOfIdSeekBuilder.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final String DEFAULT_INDEX_FILENAME = "id_seek.index";

	public static final int DEFAULT_SPLIT_LINE = 2000000;

	// About the line padding length:
	// Last value of seek: 1 271 176 034 => 10 => an upper bound is 11
	public static final int DEFAULT_PADDING_LENGTH = 11;

	/**
	 * Builds the index.
	 *
	 * @param geonamesMainFile the geonames main file
	 * @param outputDirectory the output directory
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void buildIndex(final File geonamesMainFile, final File outputDirectory) throws IOException
	{
		buildIndex(geonamesMainFile, outputDirectory, DEFAULT_SPLIT_LINE, DEFAULT_PADDING_LENGTH);
	}

	/**
	 * Builds the index.
	 *
	 * @param geonamesMainFile the geonames main file
	 * @param outputDirectory the output directory
	 * @param splitLimit the split limit
	 * @param paddingLength the padding length
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void buildIndex(final File geonamesMainFile, final File outputDirectory, final int splitLimit, final int paddingLength) throws IOException
	{
		logger.debug("Build index id->seek starting...");

		File target = new File(outputDirectory.getAbsoluteFile() + File.separator + DEFAULT_INDEX_FILENAME);

		geonamesToIdSeek(geonamesMainFile, target);

		StringFileSorter.sortBigStringFile(target, splitLimit, new StringLineIdComparator());

		padFile(target, paddingLength);

		logger.debug("Build index id->seek done.");
	}

	/**
	 * Geonames to id seek.
	 *
	 * @param source the source
	 * @param target the target
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void geonamesToIdSeek(final File source, final File target) throws IOException
	{
		logger.debug("geonamesToIdSeek starting...");

		Chronometer chronoStep = new Chronometer();

		long currentSeekValue = 0;
		BufferedReader in = null;
		PrintWriter out = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			boolean ended = false;
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("\tlineCount=" + lineCount + "\tcurrentSeekValue=" + currentSeekValue + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t"
							+ ((int) (chronoStep.stop().interval() / 1000)) + " s");
					chronoStep.reset();
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					String id = line.split("\t")[0];

					out.println(id + "\t" + currentSeekValue);

					currentSeekValue += line.getBytes().length + 1;
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}

		logger.debug("geonamesToIdSeek done.");
	}

	/**
	 * Pad file.
	 *
	 * @param source the source
	 * @param target the target
	 * @param paddingLength the padding length
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void padFile(final File source, final File target, final int paddingLength) throws IOException
	{
		Chronometer chronoStep = new Chronometer();

		BufferedReader in = null;
		PrintWriter out = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			int lineCount = 0;
			int currentId = 0;
			boolean ended = false;
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("\tlineCount=" + lineCount + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t" + ((int) (chronoStep.stop().interval() / 1000)) + " s");
					chronoStep.reset();
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;
					currentId += 1;

					String[] tokens = line.split("\t");
					int id = Integer.parseInt(tokens[0]);
					long seekValue = Long.parseLong(tokens[1]);

					while (currentId < id)
					{
						out.println(StringUtils.rightPad("", paddingLength));

						currentId += 1;
					}

					String newLine = StringUtils.rightPad(String.valueOf(seekValue), paddingLength);

					out.println(newLine);
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * Pad file.
	 *
	 * @param source the source
	 * @param paddingLength the padding length
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void padFile(final File source, final int paddingLength) throws IOException
	{
		File target = new File(source.getAbsolutePath() + ".padded" + System.currentTimeMillis());

		padFile(source, target, paddingLength);

		target.renameTo(source);
	}
}
