#!/bin/sh

# Set the current directory with the FlatDB4GeoNames directory.
cd "`dirname "$0"`"

#
echo "Information about default installed Java:"
java -version

# Launching with the default JVM.
java -jar flatdb4geonames.jar $@
