/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.util.Comparator;

/**
 * The Class StringLineIdComparator.
 */
public class StringLineIdComparator implements Comparator<String>
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare(final String alpha, final String bravo)
	{
		int result;

		//
		Integer alphaValue;
		if (alpha == null)
		{
			alphaValue = null;
		}
		else
		{
			alphaValue = Integer.valueOf(alpha.split("\t")[0]);
		}

		//
		Integer bravoValue;
		if (bravo == null)
		{
			bravoValue = null;
		}
		else
		{
			bravoValue = Integer.valueOf(bravo.split("\t")[0]);
		}

		//
		result = compare(alphaValue, bravoValue);

		//
		return result;
	}

	/**
	 * This method compares two nullable string values.
	 * 
	 * The comparison manages the local language alphabet order.
	 * 
	 * <pre>
	 * {@code 
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * }
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Integer alpha, final Integer bravo)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = +1;
		}
		else if (bravo == null)
		{
			result = -1;
		}
		else
		{
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}
}
