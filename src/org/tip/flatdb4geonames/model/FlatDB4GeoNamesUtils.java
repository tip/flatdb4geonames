/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FlatDB4GeoNamesUtils.
 */
public class FlatDB4GeoNamesUtils
{
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesUtils.class);

	/**
	 * Cpm: copy/paste from commons-collection4.4.0. Use of 3.2.2 to be
	 * compatible with commons-beanutils.
	 * 
	 * Returns <code>true</code> iff all elements of {@code coll2} are also
	 * contained in {@code coll1}. The cardinality of values in {@code coll2} is
	 * not taken into account, which is the same behavior as
	 * {@link Collection#containsAll(Collection)}.
	 * <p>
	 * In other words, this method returns <code>true</code> iff the
	 * intersection of <i>coll1</i> and <i>coll2</i> has the same cardinality as
	 * the set of unique values from {@code coll2}. In case {@code coll2} is
	 * empty, {@code true} will be returned.
	 * <p>
	 * This method is intended as a replacement for
	 * {@link Collection#containsAll(Collection)} with a guaranteed runtime
	 * complexity of {@code O(n + m)}. Depending on the type of
	 * {@link Collection} provided, this method will be much faster than calling
	 * {@link Collection#containsAll(Collection)} instead, though this will come
	 * at the cost of an additional space complexity O(n).
	 *
	 * @param coll1
	 *            the first collection, must not be null
	 * @param coll2
	 *            the second collection, must not be null
	 * @return <code>true</code> iff the intersection of the collections has the
	 *         same cardinality as the set of unique elements from the second
	 *         collection
	 * @since 4.0
	 */
	public static boolean containsAll(final Collection<?> coll1, final Collection<?> coll2)
	{
		if (coll2.isEmpty())
		{
			return true;
		}
		else
		{
			final Iterator<?> it = coll1.iterator();
			final Set<Object> elementsAlreadySeen = new HashSet<Object>();
			for (final Object nextElement : coll2)
			{
				if (elementsAlreadySeen.contains(nextElement))
				{
					continue;
				}

				boolean foundCurrentElement = false;
				while (it.hasNext())
				{
					final Object p = it.next();
					elementsAlreadySeen.add(p);
					if (nextElement == null ? p == null : nextElement.equals(p))
					{
						foundCurrentElement = true;
						break;
					}
				}

				if (foundCurrentElement)
				{
					continue;
				}
				else
				{
					return false;
				}
			}
			return true;
		}
	}
}
