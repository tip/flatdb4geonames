/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui;

import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.gui.util.GUIToolBox;
import org.tip.flatdb4geonames.gui.views.FlatDB4GeoNamesWindow;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;

import fr.devinsy.strings.StringList;

/**
 * The Class FlatDB4GeoNamesGUI.
 */
public final class FlatDB4GeoNamesGUI
{
	/** Lazy-loading with holder */
	private static class SingletonLoader
	{
		private static final FlatDB4GeoNamesGUI instance = new FlatDB4GeoNamesGUI();
	}

	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesGUI.class);

	private File homeDirectory;

	/**
	 * Launch the application.
	 */
	private FlatDB4GeoNamesGUI()
	{
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
		{
			/**
			 * 
			 */
			@Override
			public void uncaughtException(final Thread thread, final Throwable exception)
			{
				if (exception instanceof OutOfMemoryError)
				{
					logger.error(exception.getMessage(), exception);
					String message = "Java ran out of memory! Increase it.";
					JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (exception instanceof FlatDB4GeoNamesException)
				{
					logger.error(exception.getMessage(), exception);
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
				}
				else
				{
					logger.error(exception.getMessage(), exception);
					StringList message = new StringList();
					message.append("A system error occured: ");
					message.appendln(exception.getMessage());
					message.appendln("Call developer.");
					JOptionPane.showMessageDialog(null, message.toString(), "System Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		// Set home directory.
		this.homeDirectory = new File(System.getProperty("user.home") + File.separator + ".flatdb4geonames");
		if (!this.homeDirectory.exists())
		{
			//
			this.homeDirectory.mkdir();
			logger.info("Home directory created: " + this.homeDirectory.getAbsolutePath());
		}

		// Remove BOLD on default font.
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		// Set LookAndFeel.
		logger.info("System lookAndFeel property: {}", System.getProperty("swing.defaultlaf"));
		logger.info("Available lookAndFeel: {}", GUIToolBox.availableLookAndFeels().toString());
		logger.info("System lookAndFeel: {}", UIManager.getSystemLookAndFeelClassName());
		logger.info("Current lookAndFeel: {}", UIManager.getLookAndFeel().getName());

		if (!StringUtils.equals(UIManager.getSystemLookAndFeelClassName(), "javax.swing.plaf.metal.MetalLookAndFeel"))
		{
			try
			{
				logger.info("Metal LAF setted and system LAF detected, try to set system LAF.");
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
			catch (final Exception exception)
			{
				logger.info("Failed to set the system LookAndFeel.");
			}
		}
		else if (GUIToolBox.availableLookAndFeels().toString().contains("GTK+"))
		{
			try
			{
				logger.info("Metal LAF setted and GTK+ LAF detected, try to set GTK+ LAF.");
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			}
			catch (final Exception exception)
			{
				logger.info("Failed to set the system LookAndFeel.");
			}
		}

		logger.info("Activated lookAndFeel: " + UIManager.getLookAndFeel().getName());
	}

	/**
	 * Exit.
	 */
	public void exit()
	{
		System.exit(0);
	}

	/**
	 * Run.
	 */
	public void run()
	{
		new FlatDB4GeoNamesWindow().setVisible(true);
	}

	/**
	 * Instance.
	 *
	 * @return the flatDB4GeoNamesGUI
	 */
	public static FlatDB4GeoNamesGUI instance()
	{
		FlatDB4GeoNamesGUI result;

		result = SingletonLoader.instance;

		//
		return result;
	}
}
