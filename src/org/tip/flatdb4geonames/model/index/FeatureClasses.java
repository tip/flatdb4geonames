/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FeatureClasses.
 */
public class FeatureClasses extends ArrayList<FeatureClass>
{
	private static final long serialVersionUID = -970464658404416401L;

	private static Logger logger = LoggerFactory.getLogger(FeatureClasses.class);

	/**
	 * Instantiates a new feature classes.
	 */
	public FeatureClasses()
	{
		super();
	}

	/**
	 * Instantiates a new feature classes.
	 *
	 * @param source
	 *            the source
	 */
	public FeatureClasses(final FeatureClass[] source)
	{
		super(source.length);

		for (FeatureClass featureClass : source)
		{
			add(featureClass);
		}
	}

	/**
	 * Instantiates a new feature classes.
	 *
	 * @param source
	 *            the source
	 */
	public FeatureClasses(final FeatureClasses source)
	{
		super(source.size());

		for (FeatureClass featureClass : source)
		{
			add(featureClass);
		}
	}

	/**
	 * Instantiates a new feature classes.
	 *
	 * @param capacity
	 *            the capacity
	 */
	public FeatureClasses(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Adds the all.
	 *
	 * @param source
	 *            the source
	 */
	public void addAll(final FeatureClass[] source)
	{
		if (source != null)
		{
			for (FeatureClass featureClass : source)
			{
				add(featureClass);
			}
		}
	}

	/**
	 * Contains.
	 *
	 * @param target
	 *            the target
	 * @return true, if successful
	 */
	public boolean contains(final FeatureClass target)
	{
		boolean result;

		if (target == null)
		{
			result = false;
		}
		else
		{
			boolean ended = false;
			Iterator<FeatureClass> iterator = this.iterator();
			result = false;
			while (!ended)
			{
				if (iterator.hasNext())
				{
					FeatureClass current = iterator.next();

					if (current == target)
					{
						ended = true;
						result = true;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}
}
