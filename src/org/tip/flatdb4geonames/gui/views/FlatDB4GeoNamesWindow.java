/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.gui.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.gui.FlatDB4GeoNamesGUI;
import org.tip.flatdb4geonames.gui.views.buildindexes.BuildIndexesDialog;
import org.tip.flatdb4geonames.gui.views.downloadgeonamesfiles.DownloadGeoNamesFilesDialog;
import org.tip.flatdb4geonames.gui.views.help.AboutDialog;
import org.tip.flatdb4geonames.gui.views.help.TutorialDialog;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.workers.StatisticsReporter;
import org.tip.flatdb4geonames.swing.FlatDB4GeoNamesGeneralPanel;
import org.tip.flatdb4geonames.swing.FlatDB4GeoNamesRequestPanel;
import org.tip.flatdb4geonames.swing.FlatDB4GeoNamesStatisticsPanel;
import org.tip.flatdb4geonames.swing.downloaddatabase.DownloadDatabaseDialog;

import fr.devinsy.strings.StringList;

/**
 * The Class FlatDB4GeoNamesWindow.
 */
public class FlatDB4GeoNamesWindow extends JFrame
{
	private static final long serialVersionUID = 5041543095300009538L;
	private static Logger logger = LoggerFactory.getLogger(FlatDB4GeoNamesWindow.class);

	private JPanel contentPane;
	private FlatDB4GeoNamesGeneralPanel generalTab;
	private FlatDB4GeoNamesRequestPanel requestTab;
	private JTabbedPane tabbedPane;

	/**
	 * Instantiates a new FlatDB4GeoNames window.
	 */
	public FlatDB4GeoNamesWindow()
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(FlatDB4GeoNamesWindow.class.getResource("/org/tip/flatdb4geonames/gui/favicon-16x16.jpg")));
		setTitle("FlatDB4GeoNames");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 593, 482);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		JMenuItem mntmBuildIndex = new JMenuItem("Build indexes…");
		mntmBuildIndex.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Build indexes…
				BuildIndexesDialog.showDialog();
			}
		});

		JMenuItem mntmDownloadGeonamesFiles = new JMenuItem("Download GeoNames files…");
		mntmDownloadGeonamesFiles.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Download GeoNames files…
				DownloadGeoNamesFilesDialog.showDialog(null);
			}
		});
		mnFile.add(mntmDownloadGeonamesFiles);
		mnFile.add(mntmBuildIndex);

		JSeparator separator3 = new JSeparator();
		mnFile.add(separator3);

		JMenuItem mntmDownloadIndexes = new JMenuItem("Download full database…");
		mntmDownloadIndexes.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Download Database files…
				DownloadDatabaseDialog.showDialog();
			}
		});
		mnFile.add(mntmDownloadIndexes);

		JSeparator separator2 = new JSeparator();
		mnFile.add(separator2);

		JMenuItem mntmOpenDatabase = new JMenuItem("Open database…");
		mntmOpenDatabase.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Open database…
				try
				{
					File databaseDirectory = DatabaseDirectorySelector.showSelectorDialog(null, null);

					if (databaseDirectory != null)
					{
						FlatDB4GeoNames.open(databaseDirectory);
					}
				}
				catch (IOException exception)
				{
					//
					logger.error(exception.getMessage(), exception);

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
				finally
				{
					refreshAll();
				}
			}
		});
		mnFile.add(mntmOpenDatabase);

		JMenuItem mntmCloseDatabase = new JMenuItem("Close database");
		mntmCloseDatabase.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Close database.
				FlatDB4GeoNames.close();

				refreshAll();
			}
		});
		mnFile.add(mntmCloseDatabase);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Quit.
				FlatDB4GeoNamesGUI.instance().exit();
			}
		});
		mntmQuit.setMnemonic('Q');
		mnFile.add(mntmQuit);

		JMenu mnOperation = new JMenu("Operation");
		menuBar.add(mnOperation);

		JMenuItem mntmGeneral = new JMenuItem("General");
		mntmGeneral.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// General.
				FlatDB4GeoNamesWindow.this.tabbedPane.setSelectedIndex(0);
			}
		});
		mnOperation.add(mntmGeneral);

		JMenuItem mntmRequest = new JMenuItem("Request");
		mntmRequest.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Request.
				FlatDB4GeoNamesWindow.this.tabbedPane.setSelectedIndex(1);
			}
		});
		mnOperation.add(mntmRequest);

		JMenu mnStatistics = new JMenu("Statistics");
		menuBar.add(mnStatistics);

		JMenuItem mntmWordLength = new JMenuItem("Longest words");
		mntmWordLength.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				try
				{
					// Word length statistics.
					if (FlatDB4GeoNames.isOpened())
					{
						StringList report = StatisticsReporter.reportLongestWords(500);

						FlatDB4GeoNamesWindow.this.tabbedPane.add("Work Length Stats", new FlatDB4GeoNamesStatisticsPanel(report));

						FlatDB4GeoNamesWindow.this.tabbedPane.setSelectedIndex(FlatDB4GeoNamesWindow.this.tabbedPane.getComponentCount() - 1);
					}
				}
				catch (IOException exception)
				{
					//
					logger.error(exception.getMessage(), exception);

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnStatistics.add(mntmWordLength);

		JMenuItem mntmWordMatching = new JMenuItem("Word matching");
		mntmWordMatching.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Word matching statistics.
				try
				{
					if (FlatDB4GeoNames.isOpened())
					{
						StringList report = StatisticsReporter.reportWordMatching(200);

						FlatDB4GeoNamesWindow.this.tabbedPane.add("Work Matching Stats", new FlatDB4GeoNamesStatisticsPanel(report));

						FlatDB4GeoNamesWindow.this.tabbedPane.setSelectedIndex(FlatDB4GeoNamesWindow.this.tabbedPane.getComponentCount() - 1);
					}
				}
				catch (IOException exception)
				{
					//
					logger.error(exception.getMessage(), exception);

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnStatistics.add(mntmWordMatching);

		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);

		JMenuItem mntmHelp = new JMenuItem("Tutorial");
		mntmHelp.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Tutorial Dialog.
				TutorialDialog.showDialog();
			}
		});
		mnHelp.add(mntmHelp);

		JSeparator separator1 = new JSeparator();
		mnHelp.add(separator1);

		JMenuItem mntmAboutFlatdbgeonames = new JMenuItem("About FlatDB4GeoNames");
		mntmAboutFlatdbgeonames.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// About Dialog.
				AboutDialog.showDialog();
			}
		});
		mnHelp.add(mntmAboutFlatdbgeonames);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		this.tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		this.contentPane.add(this.tabbedPane, BorderLayout.CENTER);

		this.generalTab = new FlatDB4GeoNamesGeneralPanel();
		this.tabbedPane.addTab("General", null, this.generalTab, null);

		this.requestTab = new FlatDB4GeoNamesRequestPanel();
		this.tabbedPane.addTab("Request", null, this.requestTab, null);
	}

	/**
	 * Refresh all.
	 */
	public void refreshAll()
	{
		try
		{
			this.generalTab.refresh();
			this.requestTab.refresh();
		}
		catch (IOException exception)
		{
			logger.error(exception.getMessage(), exception);
		}
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			/* (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run()
			{
				try
				{
					FlatDB4GeoNamesWindow frame = new FlatDB4GeoNamesWindow();
					frame.setVisible(true);
				}
				catch (Exception event)
				{
					event.printStackTrace();
				}
			}
		});
	}
}
