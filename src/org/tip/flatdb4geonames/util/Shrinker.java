/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.strings.StringList;

/**
 * The Class Shrinker.
 */
public class Shrinker
{
	private static Logger logger = LoggerFactory.getLogger(Shrinker.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Shrink.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 */
	public static StringList shrink(final StringList source)
	{
		StringList result;

		result = new StringList(source.size());

		source.sort();

		logger.debug("sorted");

		boolean ended = false;
		StringList currentLine = null;
		String currentKey = null;
		int lineCount = 0;
		while (!ended)
		{
			if (lineCount < source.size())
			{
				//
				if (currentKey == null)
				{
					String line = source.get(lineCount);
					String[] tokens = line.split("\t");
					currentKey = tokens[0];

					currentLine = new StringList();
					currentLine.add(line);
				}
				else
				{
					String line = source.get(lineCount);
					String[] tokens = line.split("\t");
					String key = tokens[0];
					String ids = tokens[1];

					if (StringUtils.equals(currentKey, key))
					{
						currentLine.append(",").append(ids);
					}
					else
					{
						result.add(currentLine.toString());

						currentLine = new StringList();
						currentLine.add(line);
						currentKey = key;
					}
				}

				//
				lineCount += 1;
			}
			else
			{
				ended = true;
			}
		}

		logger.debug("shrinked: {} -> {}", source.size(), result.size());

		//
		return result;
	}

	/**
	 * Shrink.
	 * 
	 * @param in
	 *            the in
	 * @param out
	 *            the out
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void shrink(final StringListReader in, final PrintWriter out) throws IOException
	{
		boolean ended = false;
		StringList currentLine = null;
		String currentWord = null;
		int lineCount = 0;
		int shrinkCount = 0;
		while (!ended)
		{
			String line = in.readLine();

			if (line == null)
			{
				ended = true;
				if (currentLine != null)
				{
					out.println(currentLine.toString());
				}
			}
			else
			{
				//
				if (currentWord == null)
				{
					String[] tokens = line.split("\t");
					currentWord = tokens[0];

					currentLine = new StringList();
					currentLine.add(line);
				}
				else
				{
					String[] tokens = line.split("\t");
					String word = tokens[0];
					String ids = tokens[1];

					if (StringUtils.equals(currentWord, word))
					{
						currentLine.append(",").append(ids);
						shrinkCount += 1;
					}
					else
					{
						out.println(currentLine.toString());

						currentLine = new StringList();
						currentLine.add(line);
						currentWord = word;
					}
				}

				lineCount += 1;
			}
		}

		logger.debug("shrinked: " + lineCount + " -> " + (lineCount - shrinkCount));
	}

	/**
	 * Shrink concatentating.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void shrinkConcatentating(final Files source, final File target) throws IOException
	{
		FileSortingConcatenator in = null;
		PrintWriter out = null;
		try
		{
			in = new FileSortingConcatenator(source);

			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			shrink(in, out);
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * Shrink string file.
	 * 
	 * @param source
	 *            the source
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void shrinkStringFile(final File source) throws IOException
	{
		File target = new File(source.getAbsolutePath() + ".shrinked" + System.currentTimeMillis());

		shrinkStringFile(source, target);

		target.renameTo(source);
	}

	/**
	 * Shrink string file.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void shrinkStringFile(final File source, final File target) throws IOException
	{
		BufferedStringListReader in = null;
		PrintWriter out = null;
		try
		{
			in = new BufferedStringListReader(new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME)));
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			shrink(in, out);
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}
}
