/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.FeatureClass;

import fr.devinsy.strings.StringList;
import fr.devinsy.strings.StringSet;

/**
 * The Class GeoNamesLine.
 * 
 * Extract of http://download.geonames.org/export/dump/readme.txt
 * 
 * <pre>
 * The main 'geoname' table has the following fields :
 * 		---------------------------------------------------
 * 		geonameid         : integer id of record in geonames database
 * 		name              : name of geographical point (utf8) varchar(200)
 * 		asciiname         : name of geographical point in plain ascii characters, varchar(200)
 * 		alternatenames    : alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)
 * 		latitude          : latitude in decimal degrees (wgs84)
 * 		longitude         : longitude in decimal degrees (wgs84)
 * 		feature class     : see http://www.geonames.org/export/codes.html, char(1)
 * 		feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
 * 		country code      : ISO-3166 2-letter country code, 2 characters
 * 		cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
 * 		admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
 * 		admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
 * 		admin3 code       : code for third level administrative division, varchar(20)
 * 		admin4 code       : code for fourth level administrative division, varchar(20)
 * 		population        : bigint (8 byte int) 
 * 		elevation         : in meters, integer
 * 		dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
 * 		timezone          : the timezone id (see file timeZone.txt) varchar(40)
 * 		modification date : date of last modification in yyyy-MM-dd format
 * </pre>
 * 
 */
public class GeoNamesLine
{
	private static Logger logger = LoggerFactory.getLogger(GeoNamesLine.class);

	private long geoNameId;
	private String name;
	private String asciiName;
	private StringSet alternateNames;
	private Double latitude;
	private Double longitude;
	private FeatureClass featureClass;
	private String featureCode;
	private String countryCode;
	private StringSet alternateCountryCodes;
	private String administrativeCode1;
	private String administrativeCode2;
	private String administrativeCode3;
	private String administrativeCode4;
	private Long population;
	private Integer elevation;
	private Integer dem;
	private String timeZone;
	private String modificationDate;

	/**
	 * Instantiates a new geo names line.
	 */
	public GeoNamesLine()
	{
		this.alternateNames = new StringSet();
		this.alternateCountryCodes = new StringSet();
	}

	/**
	 * Gets the administrative code 1.
	 * 
	 * @return the administrative code 1
	 */
	public String getAdministrativeCode1()
	{
		return this.administrativeCode1;
	}

	/**
	 * Gets the administrative code 2.
	 * 
	 * @return the administrative code 2
	 */
	public String getAdministrativeCode2()
	{
		return this.administrativeCode2;
	}

	/**
	 * Gets the administrative code 3.
	 * 
	 * @return the administrative code 3
	 */
	public String getAdministrativeCode3()
	{
		return this.administrativeCode3;
	}

	/**
	 * Gets the administrative code 4.
	 * 
	 * @return the administrative code 4
	 */
	public String getAdministrativeCode4()
	{
		return this.administrativeCode4;
	}

	/**
	 * Gets the administrative code path.
	 * 
	 * @return the administrative code path
	 */
	public String getAdministrativeCodePath()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(this.countryCode);
		buffer.append(this.administrativeCode1);
		buffer.append(this.administrativeCode2);
		buffer.append(this.administrativeCode3);
		buffer.append(this.administrativeCode4);

		result = buffer.toStringSeparatedBy(".");

		//
		return result;
	}

	/**
	 * Gets the administrative short code path.
	 * 
	 * @return the administrative short code path
	 */
	public String getAdministrativeShortCodePath()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(this.countryCode);
		buffer.append(this.administrativeCode1);
		buffer.append(this.administrativeCode2);

		result = buffer.toStringSeparatedBy(".");

		//
		return result;
	}

	/**
	 * Gets the alternate country codes.
	 * 
	 * @return the alternate country codes
	 */
	public StringSet getAlternateCountryCodes()
	{
		return this.alternateCountryCodes;
	}

	/**
	 * Gets the alternate names.
	 * 
	 * @return the alternate names
	 */
	public StringSet getAlternateNames()
	{
		return this.alternateNames;
	}

	/**
	 * Gets the ascii name.
	 * 
	 * @return the ascii name
	 */
	public String getAsciiName()
	{
		return this.asciiName;
	}

	/**
	 * Gets the country code.
	 * 
	 * @return the country code
	 */
	public String getCountryCode()
	{
		return this.countryCode;
	}

	/**
	 * Gets the dem.
	 * 
	 * @return the dem
	 */
	public Integer getDem()
	{
		return this.dem;
	}

	/**
	 * Gets the elevation.
	 * 
	 * @return the elevation
	 */
	public Integer getElevation()
	{
		return this.elevation;
	}

	/**
	 * Gets the feature class.
	 * 
	 * @return the feature class
	 */
	public FeatureClass getFeatureClass()
	{
		return this.featureClass;
	}

	/**
	 * Gets the feature code.
	 * 
	 * @return the feature code
	 */
	public String getFeatureCode()
	{
		return this.featureCode;
	}

	/**
	 * Gets the feature code path.
	 * 
	 * @return the feature code path
	 */
	public String getFeatureCodePath()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(this.featureClass.getCode());
		buffer.append(this.featureCode);

		result = buffer.toStringSeparatedBy(".");

		//
		return result;
	}

	/**
	 * Gets the geo name id.
	 * 
	 * @return the geo name id
	 */
	public long getGeoNameId()
	{
		return this.geoNameId;
	}

	/**
	 * Gets the latitude.
	 * 
	 * @return the latitude
	 */
	public Double getLatitude()
	{
		return this.latitude;
	}

	/**
	 * Gets the longitude.
	 * 
	 * @return the longitude
	 */
	public Double getLongitude()
	{
		return this.longitude;
	}

	/**
	 * Gets the modification date.
	 * 
	 * @return the modification date
	 */
	public String getModificationDate()
	{
		return this.modificationDate;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Gets the population.
	 * 
	 * @return the population
	 */
	public Long getPopulation()
	{
		return this.population;
	}

	/**
	 * Gets the time zone.
	 * 
	 * @return the time zone
	 */
	public String getTimeZone()
	{
		return this.timeZone;
	}

	/**
	 * Sets the admin code 1.
	 * 
	 * @param adminCode1
	 *            the new admin code 1
	 */
	public void setAdminCode1(final String adminCode1)
	{
		this.administrativeCode1 = adminCode1;
	}

	/**
	 * Sets the admin code 2.
	 * 
	 * @param adminCode2
	 *            the new admin code 2
	 */
	public void setAdminCode2(final String adminCode2)
	{
		this.administrativeCode2 = adminCode2;
	}

	/**
	 * Sets the admin code 3.
	 * 
	 * @param adminCode3
	 *            the new admin code 3
	 */
	public void setAdminCode3(final String adminCode3)
	{
		this.administrativeCode3 = adminCode3;
	}

	/**
	 * Sets the admin code 4.
	 * 
	 * @param adminCode4
	 *            the new admin code 4
	 */
	public void setAdminCode4(final String adminCode4)
	{
		this.administrativeCode4 = adminCode4;
	}

	/**
	 * Sets the ascii name.
	 * 
	 * @param asciiName
	 *            the new ascii name
	 */
	public void setAsciiName(final String asciiName)
	{
		this.asciiName = asciiName;
	}

	/**
	 * Sets the country code.
	 * 
	 * @param countryCode
	 *            the new country code
	 */
	public void setCountryCode(final String countryCode)
	{
		this.countryCode = countryCode;
	}

	/**
	 * Sets the dem.
	 * 
	 * @param dem
	 *            the new dem
	 */
	public void setDem(final Integer dem)
	{
		this.dem = dem;
	}

	/**
	 * Sets the elevation.
	 * 
	 * @param elevation
	 *            the new elevation
	 */
	public void setElevation(final Integer elevation)
	{
		this.elevation = elevation;
	}

	/**
	 * Sets the feature class.
	 * 
	 * @param featureClass
	 *            the new feature class
	 */
	public void setFeatureClass(final FeatureClass featureClass)
	{
		this.featureClass = featureClass;
	}

	/**
	 * Sets the feature code.
	 * 
	 * @param featureCode
	 *            the new feature code
	 */
	public void setFeatureCode(final String featureCode)
	{
		this.featureCode = featureCode;
	}

	/**
	 * Sets the geo name id.
	 * 
	 * @param geoNameId
	 *            the new geo name id
	 */
	public void setGeoNameId(final long geoNameId)
	{
		this.geoNameId = geoNameId;
	}

	/**
	 * Sets the latitude.
	 * 
	 * @param latitude
	 *            the new latitude
	 */
	public void setLatitude(final Double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * Sets the longitude.
	 * 
	 * @param longitude
	 *            the new longitude
	 */
	public void setLongitude(final Double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * Sets the modification date.
	 * 
	 * @param modificationDate
	 *            the new modification date
	 */
	public void setModificationDate(final String modificationDate)
	{
		this.modificationDate = modificationDate;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * Sets the population.
	 * 
	 * @param population
	 *            the new population
	 */
	public void setPopulation(final Long population)
	{
		this.population = population;
	}

	/**
	 * Sets the time zone.
	 * 
	 * @param timeZone
	 *            the new time zone
	 */
	public void setTimeZone(final String timeZone)
	{
		this.timeZone = timeZone;
	}

	/**
	 * To line.
	 * 
	 * @return the string
	 */
	public String toLine()
	{
		String result;

		StringList buffer = new StringList(50);

		buffer.append(this.geoNameId);
		buffer.append('\t').append(this.name);
		buffer.append('\t').append(this.asciiName);
		buffer.append('\t').append(this.alternateNames.toStringList().toStringWithCommas());
		if (this.latitude == null)
		{
			buffer.append('\t');
		}
		else
		{
			buffer.append('\t').append(this.latitude);
		}
		if (this.longitude == null)
		{
			buffer.append('\t');
		}
		else
		{
			buffer.append('\t').append(this.longitude);
		}
		buffer.append('\t').append(this.featureClass.getCode());
		buffer.append('\t').append(this.featureCode);
		buffer.append('\t').append(this.countryCode);
		buffer.append('\t').append(this.alternateCountryCodes.toStringList().toStringWithCommas());
		buffer.append('\t').append(this.administrativeCode1);
		buffer.append('\t').append(this.administrativeCode2);
		buffer.append('\t').append(this.administrativeCode3);
		buffer.append('\t').append(this.administrativeCode4);
		if (this.population == null)
		{
			buffer.append('\t');
		}
		else
		{
			buffer.append('\t').append(this.population);
		}
		if (this.elevation == null)
		{
			buffer.append('\t');
		}
		else
		{
			buffer.append('\t').append(this.elevation);
		}
		if (this.dem == null)
		{
			buffer.append('\t');
		}
		else
		{
			buffer.append('\t').append(this.dem);
		}
		buffer.append('\t').append(this.timeZone);
		buffer.append('\t').append(this.modificationDate);

		result = buffer.toString();

		//
		return result;
	}
}
