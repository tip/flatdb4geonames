/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model;

import java.io.IOException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.index.FeatureClass;

/**
 * The Class GeoNamesFlatDatabaseTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GeoNamesFlatDatabaseTest
{
	private static Logger logger = LoggerFactory.getLogger(GeoNamesFlatDatabaseTest.class);

	public static final String DATABASE_HOME = "data/";

	/**
	 * Test search feature description.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchFeatureDescription() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		Assertions.assertThat(database.searchFeatureDescription(null)).isNull();
		Assertions.assertThat(database.searchFeatureDescription("X")).isNull();
		Assertions.assertThat(database.searchFeatureDescription("H")).isNull();
		Assertions.assertThat(database.searchFeatureDescription("H.CNLA")).isEqualTo("a conduit used to carry water");
		Assertions.assertThat(database.searchFeatureDescription("H.FOO")).isNull();

		Assertions.assertThat(database.searchFeatureDescription(null, null)).isNull();
		Assertions.assertThat(database.searchFeatureDescription("X", null)).isNull();
		Assertions.assertThat(database.searchFeatureDescription("H", null)).isNull();
		Assertions.assertThat(database.searchFeatureDescription("H", "CNLA")).isEqualTo("a conduit used to carry water");
		Assertions.assertThat(database.searchFeatureDescription("H", "FOO")).isNull();

	}

	/**
	 * Test search feature description path.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchFeatureDescriptionPath() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		Assertions.assertThat(database.searchFeatureDescriptionPath(null)).isNull();
		Assertions.assertThat(database.searchFeatureDescriptionPath("X")).isNull();
		Assertions.assertThat(database.searchFeatureDescriptionPath("H")).isEqualTo("Stream, lake…");
		Assertions.assertThat(database.searchFeatureDescriptionPath("CNLA")).isEqualTo("Stream, lake… / aqueduct");
		Assertions.assertThat(database.searchFeatureDescriptionPath("H.CNLA")).isEqualTo("Stream, lake… / aqueduct");
		Assertions.assertThat(database.searchFeatureDescriptionPath("H.FOO")).isNull();

		Assertions.assertThat(database.searchFeatureDescriptionPath(null, null)).isNull();
		Assertions.assertThat(database.searchFeatureDescriptionPath("X", null)).isNull();
		Assertions.assertThat(database.searchFeatureDescriptionPath("H", null)).isEqualTo("Stream, lake…");
		Assertions.assertThat(database.searchFeatureDescriptionPath("H", "CNLA")).isEqualTo("Stream, lake… / aqueduct");
		Assertions.assertThat(database.searchFeatureDescriptionPath("H", "FOO")).isNull();
	}

	/**
	 * Test search feature short description.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchFeatureShortDescription() throws IOException, FlatDB4GeoNamesException
	{
		FlatDB4GeoNames.open(DATABASE_HOME);

		GeoNamesFlatDatabase database = FlatDB4GeoNames.instance();

		Assertions.assertThat(database.searchFeatureShortDescription(null)).isNull();
		Assertions.assertThat(database.searchFeatureShortDescription("X")).isNull();
		Assertions.assertThat(database.searchFeatureShortDescription("H")).isEqualTo(FeatureClass.valueOfCode("H").getDescription());
		Assertions.assertThat(database.searchFeatureShortDescription("CNLA")).isEqualTo("aqueduct");
		Assertions.assertThat(database.searchFeatureShortDescription("H.CNLA")).isEqualTo("aqueduct");
		Assertions.assertThat(database.searchFeatureShortDescription("H.FOO")).isNull();

		Assertions.assertThat(database.searchFeatureShortDescription(null, null)).isNull();
		Assertions.assertThat(database.searchFeatureShortDescription("X", null)).isNull();
		Assertions.assertThat(database.searchFeatureShortDescription("H", null)).isEqualTo(FeatureClass.valueOfCode("H").getDescription());
		Assertions.assertThat(database.searchFeatureShortDescription("H", "CNLA")).isEqualTo("aqueduct");
		Assertions.assertThat(database.searchFeatureShortDescription("H", "FOO")).isNull();
	}
}
