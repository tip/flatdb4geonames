/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Comparator;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FileSortingConcatenator.
 */
public class FileSortingConcatenator implements StringListReader
{
	private static Logger logger = LoggerFactory.getLogger(FileSortingConcatenator.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private SingleLineBufferedFileReaders readers;
	private SingleLineBufferedFileReaderComparator comparator;

	/**
	 * Instantiates a new file sorting concatenator.
	 * 
	 * @param files
	 *            the files
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public FileSortingConcatenator(final Files files) throws IOException
	{
		this(files, null);
	}

	/**
	 * Instantiates a new file sorting concatenator.
	 * 
	 * @param files
	 *            the files
	 * @param comparator
	 *            the comparator
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public FileSortingConcatenator(final Files files, final Comparator<String> comparator) throws IOException
	{
		//
		this.readers = new SingleLineBufferedFileReaders(files.size());
		this.comparator = new SingleLineBufferedFileReaderComparator(comparator);

		//
		for (File file : files)
		{
			SingleLineBufferedFileReader reader = new SingleLineBufferedFileReader(file);
			this.readers.add(reader);
		}
	}

	/* (non-Javadoc)
	 * @see org.tip.flatdb4geonames.util.StringListReader#close()
	 */
	@Override
	public void close()
	{
		for (SingleLineBufferedFileReader reader : this.readers)
		{
			reader.close();
		}
	}

	/**
	 * Read line.
	 * 
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public String readLine() throws IOException
	{
		String result;

		this.readers.sort(this.comparator);

		result = this.readers.get(0).readLine();

		//
		return result;
	}

	/**
	 * Concatenate sorting.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param comparator
	 *            the comparator
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void concatenateSorting(final Files source, final File target, final Comparator<String> comparator) throws IOException
	{
		FileSortingConcatenator in = null;
		PrintWriter out = null;
		try
		{
			in = new FileSortingConcatenator(source, comparator);

			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(target), DEFAULT_CHARSET_NAME));

			boolean ended = false;
			long lineCount = 0;
			Chronometer chrono = new Chronometer();
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("lineCount=" + lineCount + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t" + ((int) (chrono.stop().interval() / 1000)) + " s");
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					out.println(line);
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}
}
