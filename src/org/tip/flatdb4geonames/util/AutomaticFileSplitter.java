/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AutomaticFileSplitter.
 */
public class AutomaticFileSplitter
{
	private static Logger logger = LoggerFactory.getLogger(AutomaticFileSplitter.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private File referentFile;
	private Files files;
	private int limit;
	private int lineCount;
	private PrintWriter out;

	/**
	 * Instantiates a new automatic file splitter.
	 * 
	 * @param referentFile
	 *            the referent file
	 * @param limit
	 *            the limit
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AutomaticFileSplitter(final File referentFile, final int limit) throws IOException
	{
		this.lineCount = 0;
		this.referentFile = referentFile;
		this.limit = limit;
		this.files = new Files(limit);
		nextFile();
	}

	/**
	 * Close.
	 */
	public void close()
	{
		IOUtils.closeQuietly(this.out);
	}

	/**
	 * Gets the limit.
	 * 
	 * @return the limit
	 */
	public int getLimit()
	{
		return this.limit;
	}

	/**
	 * Gets the line count.
	 * 
	 * @return the line count
	 */
	public int getLineCount()
	{
		return this.lineCount;
	}

	/**
	 * Gets the output files.
	 * 
	 * @return the output files
	 */
	public Files getOutputFiles()
	{
		return this.files;
	}

	/**
	 * Gets the referent file.
	 * 
	 * @return the referent file
	 */
	public File getReferentFile()
	{
		return this.referentFile;
	}

	/**
	 * Next file.
	 * 
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	private void nextFile() throws UnsupportedEncodingException, FileNotFoundException
	{
		close();

		File file = new File(this.referentFile.getAbsolutePath() + "." + this.files.size());
		this.out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		this.files.add(file);
		this.lineCount = 0;
	}

	/**
	 * Println.
	 * 
	 * @param line
	 *            the line
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void println(final String line) throws IOException
	{
		if (line != null)
		{
			if (this.lineCount == this.limit)
			{
				logger.debug("Limit reached.");

				nextFile();
			}

			this.out.println(line);
			this.lineCount += 1;
		}
	}

	/**
	 * Split file.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @param limit
	 *            the limit
	 * @return the files
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Files splitFile(final File source, final File target, final int limit) throws IOException
	{
		Files result;

		BufferedReader in = null;
		AutomaticFileSplitter out = null;
		long lineCount = 0;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));
			out = new AutomaticFileSplitter(target, limit);

			boolean ended = false;
			Chronometer chrono = new Chronometer();
			while (!ended)
			{
				if (lineCount % 1000000 == 0)
				{
					System.gc();

					logger.debug("lineCount=" + lineCount + " \tfiles=" + out.getOutputFiles().size() + " \tmemory=" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + " Mo \t"
							+ ((int) (chrono.stop().interval() / 1000)) + " s");
				}

				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					lineCount += 1;

					out.println(line);
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
			if (out != null)
			{
				out.close();
			}
		}

		result = out.getOutputFiles();

		//
		return result;
	}

	/**
	 * Split file.
	 * 
	 * @param source
	 *            the source
	 * @param limit
	 *            the limit
	 * @return the files
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Files splitFile(final File source, final int limit) throws IOException
	{
		Files result;

		result = splitFile(source, source, limit);

		//
		return result;
	}
}
