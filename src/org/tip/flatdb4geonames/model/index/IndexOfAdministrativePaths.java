/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import fr.devinsy.strings.StringList;

/**
 * The Class IndexOfAdministrativePaths.
 */
public class IndexOfAdministrativePaths
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(IndexOfAdministrativePaths.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	private AdministrativePaths administrativePaths;
	private Map<String, AdministrativePath> index;

	/**
	 * Instantiates a new index of administrative paths.
	 *
	 * @param code1File
	 *            the code 1 file
	 * @param code2File
	 *            the code 2 file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public IndexOfAdministrativePaths(final File code1File, final File code2File) throws IOException
	{
		//
		this.administrativePaths = new AdministrativePaths();
		this.index = new HashMap<String, AdministrativePath>();

		//
		{
			BufferedReader in = null;
			try
			{
				in = new BufferedReader(new InputStreamReader(IndexOfAdministrativePaths.class.getResourceAsStream("/org/tip/flatdb4geonames/model/index/ISO_3166-1-alpha-2.properties")));

				int lineCount = 0;
				boolean ended = false;
				while (!ended)
				{
					String line = in.readLine();

					if (line == null)
					{
						ended = true;
					}
					else
					{
						// logger.debug(line);
						lineCount += 1;

						if (!line.startsWith("#"))
						{
							String[] tokens = line.split("=");

							AdministrativePath code = new AdministrativePath(tokens[0], tokens[1], null);

							this.administrativePaths.add(code);
							this.index.put(code.getTag(), code);

							// logger.debug(code.toString());
						}
					}

				}

				logger.debug("count line=" + lineCount);
			}
			finally
			{
				IOUtils.closeQuietly(in);
			}

		}

		//
		for (File source : new File[] { code1File, code2File })
		{
			BufferedReader in = null;
			try
			{
				in = new BufferedReader(new InputStreamReader(new FileInputStream(source), DEFAULT_CHARSET_NAME));

				int lineCount = 0;
				boolean ended = false;
				while (!ended)
				{
					String line = in.readLine();

					if (line == null)
					{
						ended = true;
					}
					else
					{
						// logger.debug(line);
						lineCount += 1;

						String[] tokens = line.split("\t");

						if (tokens.length == 4)
						{
							AdministrativePath code = new AdministrativePath(tokens[0], tokens[1], Integer.valueOf(tokens[3]));

							this.administrativePaths.add(code);
							this.index.put(code.getTag(), code);

							// logger.debug(code.toString());
						}
					}

				}

				logger.debug("count line=" + lineCount);
			}
			finally
			{
				IOUtils.closeQuietly(in);
			}
		}
	}

	/**
	 * Clear.
	 */
	public void clear()
	{
		this.index.clear();
		this.administrativePaths.clear();
	}

	/**
	 * Gets the.
	 *
	 * @param codePath
	 *            the code path
	 * @return the administrative path
	 */
	public AdministrativePath get(final String codePath)
	{
		AdministrativePath result;

		if (codePath == null)
		{
			result = null;
		}
		else
		{
			result = this.index.get(normalizeCodePath(codePath));
		}

		//
		return result;
	}

	/**
	 * Gets the.
	 *
	 * @param countryCode
	 *            the country code
	 * @param code1
	 *            the code 1
	 * @param code2
	 *            the code 2
	 * @param code3
	 *            the code 3
	 * @param code4
	 *            the code 4
	 * @return the administrative path
	 */
	public AdministrativePath get(final String countryCode, final String code1, final String code2, final String code3, final String code4)
	{
		AdministrativePath result;

		result = get(AdministrativePath.convertToCodePath(countryCode, code1, code2, code3, code4));

		//
		return result;
	}

	/**
	 * Gets the codes.
	 *
	 * @return the codes
	 */
	public AdministrativePaths getCodes()
	{
		AdministrativePaths result;

		result = new AdministrativePaths(this.administrativePaths);

		//
		return result;
	}

	/**
	 * Search for the toponym path of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponymPath("US")        = "United-States"
	 * searchAdministrativeToponymPath("US.KY")     = "United-States / Kentucky"
	 * searchAdministrativeToponymPath("US.KY.017") = "United-States / Kentucky / Bourbon County"
	 * </pre>
	 *
	 * @param administrativeCodePath
	 *            the administrative code path
	 * @return the string
	 */
	public String searchAdministrativeToponymPath(final String administrativeCodePath)
	{
		String result;

		if (administrativeCodePath == null)
		{
			result = null;
		}
		else
		{
			StringList codePaths = new StringList();
			StringList currentCodePath = new StringList();
			for (String code : normalizeCodePath(administrativeCodePath).split("\\."))
			{
				currentCodePath.append(code);
				codePaths.append(currentCodePath.toStringSeparatedBy("."));
			}

			StringList toponymPath = new StringList();
			for (String codePath : codePaths)
			{
				AdministrativePath path = get(codePath);

				if (path == null)
				{
					toponymPath.append("?");
				}
				else
				{
					toponymPath.append(path.getToponym());
				}
			}

			result = toponymPath.toStringSeparatedBy(" / ");
		}

		//
		return result;
	}

	/**
	 * Search for the toponym path of an administrative code path.
	 * 
	 * An administrative code path is a concatenation of administrative codes
	 * separated by point:
	 * CountryCode.adminCode1.adminCode2.adminCode3.adminCode4
	 * 
	 * Examples of administrative path:
	 * 
	 * <pre>
	 * searchAdministrativeToponymPath("US", null, null, null)  = "United-States"
	 * searchAdministrativeToponymPath("US", "KY", null, null)  = "United-States / Kentucky"
	 * searchAdministrativeToponymPath("US", "KY", "017", null) = "United-States / Kentucky / Bourbon County"
	 * searchAdministrativeToponymPath("US", "FOO", null, null) = "United-States / ?"
	 * </pre>
	 *
	 * @param countryCode
	 *            the country code
	 * @param adminCode1
	 *            the admin code 1
	 * @param adminCode2
	 *            the admin code 2
	 * @param adminCode3
	 *            the admin code 3
	 * @param adminCode4
	 *            the admin code 4
	 * @return the string
	 */
	public String searchAdministrativeToponymPath(final String countryCode, final String adminCode1, final String adminCode2, final String adminCode3, final String adminCode4)
	{
		String result;

		if (countryCode == null)
		{
			result = null;
		}
		else
		{
			String administrativeCodePaths = AdministrativePath.convertToCodePath(countryCode, adminCode1, adminCode2, adminCode3, adminCode4);
			result = searchAdministrativeToponymPath(administrativeCodePaths);
		}

		//
		return result;
	}

	/**
	 * For country, the value of admin1 is ".00" but there is no entry with this
	 * pattern in index. And there is no reason to add one.
	 *
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String normalizeCodePath(final String source)
	{
		String result;

		if ((StringUtils.length(source) == 5) && (StringUtils.endsWith(source, ".00")))
		{
			result = source.substring(0, source.lastIndexOf(".00"));
		}
		else
		{
			result = source;
		}

		//
		return result;
	}
}
