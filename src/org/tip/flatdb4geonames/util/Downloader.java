/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Downloader.
 */
public class Downloader
{
	private static Logger logger = LoggerFactory.getLogger(Downloader.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Download.
	 * 
	 * @param sourceUrl
	 *            the source url
	 * @param localTargetRepository
	 *            the local target repository
	 * @return the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static File download(final String sourceUrl, final File localTargetRepository) throws IOException
	{
		File result;

		if ((StringUtils.isBlank(sourceUrl)) || (localTargetRepository == null))
		{
			throw new IllegalArgumentException("Null parameter detected.");
		}
		else if (localTargetRepository.isFile())
		{
			throw new IllegalArgumentException("Local target repository is a file.");
		}
		else
		{
			if (sourceUrl.startsWith("http"))
			{
				URL source = new URL(sourceUrl);
				result = new File(FilenameUtils.concat(localTargetRepository.getAbsolutePath(), FilenameUtils.getName(sourceUrl)));
				FileUtils.copyURLToFile(source, result);
			}
			else
			{
				result = new File(FilenameUtils.concat(localTargetRepository.getAbsolutePath(), FilenameUtils.getName(sourceUrl)));
				FileUtils.copyFile(new File(sourceUrl), result);
			}
		}

		//
		return result;
	}
}
