/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import fr.devinsy.strings.StringList;

/**
 * The Class Feature.
 */
public class Feature
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Feature.class);

	private FeatureClass featureClass;
	private String featureCode;
	private String shortDescription;
	private String description;

	/**
	 * Instantiates a new feature.
	 *
	 * @param featureClassCode
	 *            the feature class code
	 * @param featureCode
	 *            the feature code
	 * @param shortDescription
	 *            the short description
	 * @param description
	 *            the description
	 */
	public Feature(final String featureClassCode, final String featureCode, final String shortDescription, final String description)
	{
		if (!FeatureClass.isClassCode(featureClassCode))
		{
			throw new IllegalArgumentException("Bad feature class value.");
		}
		else
		{
			this.featureClass = FeatureClass.valueOfCode(featureClassCode);
			this.featureCode = featureCode;
			this.shortDescription = shortDescription;
			this.description = description;
		}
	}

	/**
	 * Gets the code path.
	 *
	 * @return the code path
	 */
	public String getCodePath()
	{
		String result;

		result = convertToFeaturePath(this.featureClass.getCode(), this.featureCode);

		//
		return result;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Gets the description path of a this feature.
	 *
	 * @return the description path
	 */
	public String getDescriptionPath()
	{
		String result;

		if (this.featureCode == null)
		{
			result = this.shortDescription;
		}
		else
		{
			result = String.format("%s / %s", this.featureClass.getDescription(), this.shortDescription);
		}

		//
		return result;
	}

	/**
	 * Gets the feature class.
	 *
	 * @return the feature class
	 */
	public FeatureClass getFeatureClass()
	{
		return this.featureClass;
	}

	/**
	 * Gets the feature class code.
	 *
	 * @return the feature class code
	 */
	public String getFeatureClassCode()
	{
		String result;

		result = this.featureClass.getCode();

		//
		return result;
	}

	/**
	 * Gets the feature code.
	 *
	 * @return the feature code
	 */
	public String getFeatureCode()
	{
		return this.featureCode;
	}

	/**
	 * Gets the short description.
	 *
	 * @return the short description
	 */
	public String getShortDescription()
	{
		return this.shortDescription;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the new code
	 */
	public void setCode(final String code)
	{
		this.featureCode = code;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * Sets the feature class.
	 *
	 * @param featureClass
	 *            the new feature class
	 */
	public void setFeatureClass(final FeatureClass featureClass)
	{
		this.featureClass = featureClass;
	}

	/**
	 * Sets the short description.
	 *
	 * @param shortDescription
	 *            the new short description
	 */
	public void setShortDescription(final String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		String result;

		StringList buffer = new StringList();

		buffer.append(getCodePath());
		buffer.append(this.shortDescription);
		buffer.append(this.description);

		result = buffer.toStringSeparatedBy("\t");

		//
		return result;
	}

	/**
	 * Convert to codes.
	 *
	 * @param codePath
	 *            the code path
	 * @return the pair
	 */
	public static Pair<String, String> convertToCodes(final String codePath)
	{
		Pair<String, String> result;

		if (StringUtils.isBlank(codePath))
		{
			result = null;
		}
		else
		{
			String codes[] = codePath.split("\\.");

			if (codes.length < 2)
			{
				result = Pair.of(codes[0], null);
			}
			else
			{
				result = Pair.of(codes[0], codes[1]);
			}
		}

		//
		return result;
	}

	/**
	 * Convert to feature path.
	 *
	 * @param featureClassCode
	 *            the feature class code
	 * @param featureCode
	 *            the feature code
	 * @return the string
	 */
	public static String convertToFeaturePath(final String featureClassCode, final String featureCode)
	{
		String result;

		if (featureClassCode == null)
		{
			result = null;
		}
		else
		{
			StringList buffer = new StringList();
			buffer.append(featureClassCode);
			buffer.append(featureCode);

			result = buffer.toStringSeparatedBy(".");
		}

		//
		return result;
	}
}
