/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.strings.StringSet;

/**
 * The Class Features.
 */
public class Features extends ArrayList<Feature>
{
	private static final long serialVersionUID = -8546831996283740924L;

	private static Logger logger = LoggerFactory.getLogger(Features.class);

	/**
	 * Instantiates a new features.
	 */
	public Features()
	{
		super();
	}

	/**
	 * Instantiates a new features.
	 *
	 * @param source
	 *            the source
	 */
	public Features(final Features source)
	{
		super(source.size());

		for (Feature feature : source)
		{
			add(feature);
		}
	}

	/**
	 * Instantiates a new features.
	 *
	 * @param capacity
	 *            the capacity
	 */
	public Features(final int capacity)
	{
		super(capacity);
	}

	/**
	 * Gets the.
	 *
	 * @param featureClassCode
	 *            the feature class code
	 * @param featureCode
	 *            the feature code
	 * @return the feature
	 */
	public Feature get(final String featureClassCode, final String featureCode)
	{
		Feature result;

		if ((StringUtils.isBlank(featureClassCode)) || (StringUtils.isBlank(featureCode)))
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			int index = 0;
			result = null;
			while (!ended)
			{
				if (index < this.size())
				{
					Feature feature = this.get(index);
					if ((feature.getFeatureClassCode().equals(featureClassCode)) && (feature.getFeatureCode().equals(featureCode)))
					{
						ended = true;
						result = feature;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by feature class.
	 *
	 * @param featureClassCode
	 *            the feature class code
	 * @return the by feature class
	 */
	public Features getByFeatureClass(final String featureClassCode)
	{
		Features result;

		result = new Features();

		for (Feature feature : this)
		{
			if (feature.getFeatureClassCode() == featureClassCode)
			{
				result.add(feature);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the feature class codes.
	 *
	 * @return the feature class codes
	 */
	public StringSet getFeatureClassCodes()
	{
		StringSet result;

		result = new StringSet();

		for (Feature feature : this)
		{
			result.add(feature.getFeatureClassCode());
		}

		//
		return result;
	}

	/**
	 * Gets the feature codes.
	 *
	 * @return the feature codes
	 */
	public StringSet getFeatureCodes()
	{
		StringSet result;

		result = new StringSet();

		for (Feature feature : this)
		{
			result.add(feature.getFeatureCode());
		}

		//
		return result;
	}

}
