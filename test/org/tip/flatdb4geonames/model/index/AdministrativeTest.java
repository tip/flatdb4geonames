/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.index;

import java.io.IOException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;
import org.tip.flatdb4geonames.model.GeoNamesFlatDatabase;

/**
 * The Class AdministrativeTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AdministrativeTest
{
	private static Logger logger = LoggerFactory.getLogger(AdministrativeTest.class);

	public static final String DATABASE_HOME = "data/";

	/**
	 * Search administrative toponym 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void searchAdministrativeToponym01() throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.open(DATABASE_HOME);

		Assertions.assertThat(database.searchAdministrativeToponym(null, null, null, null, null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", null, null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "00", null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "FOO", null, null, null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", null, null, null)).isEqualTo("Lorraine");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", null, null)).isEqualTo("Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", "80240")).isNull();
	}

	/**
	 * Search administrative toponym 02.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void searchAdministrativeToponym02() throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.open(DATABASE_HOME);

		Assertions.assertThat(database.searchAdministrativeToponym(null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR")).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR.00")).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR.FOO")).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR.B2")).isEqualTo("Lorraine");
		Assertions.assertThat(database.searchAdministrativeToponym("FR.B2.54")).isEqualTo("Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponym("FR.B2.54")).isEqualTo("Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", "80240")).isNull();
	}

	/**
	 * Test convert to code path 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testConvertToCodePath01() throws IOException, FlatDB4GeoNamesException
	{
		Assertions.assertThat(AdministrativePath.convertToCodePath(null, null, null, null, null)).isNull();
		Assertions.assertThat(AdministrativePath.convertToCodePath("FR", null, null, null, null)).isEqualTo("FR");
		Assertions.assertThat(AdministrativePath.convertToCodePath("FR", "B2", null, null, null)).isEqualTo("FR.B2");
		Assertions.assertThat(AdministrativePath.convertToCodePath("FR", "B2", "54", null, null)).isEqualTo("FR.B2.54");
		Assertions.assertThat(AdministrativePath.convertToCodePath("FR", "B2", "54", "571", null)).isEqualTo("FR.B2.54.571");
		Assertions.assertThat(AdministrativePath.convertToCodePath("FR", "B2", "54", "571", "80240")).isEqualTo("FR.B2.54.571.80240");
	}

	/**
	 * Test search administrative toponym 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchAdministrativeToponym01() throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.open(DATABASE_HOME);

		Assertions.assertThat(database.searchAdministrativeToponym(null, null, null, null, null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", null, null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "00", null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "FOO", null, null, null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", null, null, null)).isEqualTo("Lorraine");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", null, null)).isEqualTo("Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponym("FR", "B2", "54", "571", "80240")).isNull();
	}

	/**
	 * Test search administrative toponym path 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchAdministrativeToponymPath01() throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.open(DATABASE_HOME);

		Assertions.assertThat(database.searchAdministrativeToponymPath(null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR")).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.00")).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.FOO")).isEqualTo("France / ?");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.B2")).isEqualTo("France / Lorraine");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.B2.54")).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.B2.54.571")).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle / ?");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR.B2.54.571.80240")).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle / ? / ?");
	}

	/**
	 * Test search administrative toponym path 02.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FlatDB4GeoNamesException
	 *             the flat DB 4 geo names exception
	 */
	@Test
	public void testSearchAdministrativeToponymPath02() throws IOException, FlatDB4GeoNamesException
	{
		GeoNamesFlatDatabase database = FlatDB4GeoNames.open(DATABASE_HOME);

		Assertions.assertThat(database.searchAdministrativeToponymPath(null, null, null, null, null)).isNull();
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", null, null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "00", null, null, null)).isEqualTo("France");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "FOO", null, null, null)).isEqualTo("France / ?");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "B2", null, null, null)).isEqualTo("France / Lorraine");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "B2", "54", null, null)).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "B2", "54", "571", null)).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle / ?");
		Assertions.assertThat(database.searchAdministrativeToponymPath("FR", "B2", "54", "571", "80240")).isEqualTo("France / Lorraine / Département de Meurthe-et-Moselle / ? / ?");
	}
}
