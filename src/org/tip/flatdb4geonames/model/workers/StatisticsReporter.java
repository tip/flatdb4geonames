/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.model.workers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.util.StringLongPair;
import org.tip.flatdb4geonames.util.StringLongPairs;

import fr.devinsy.strings.StringList;

/**
 * The Class StatisticsReporter.
 */
public class StatisticsReporter
{
	private static Logger logger = LoggerFactory.getLogger(StatisticsReporter.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Report longest words.
	 * 
	 * @param maxLineCount
	 *            the max line count
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList reportLongestWords(final int maxLineCount) throws IOException
	{
		StringList result;

		StringLongPairs data = StatisticsWorker.censusLongestWords(maxLineCount);

		result = new StringList();

		result.append("<H1>LONGEST WORDS</H1>");
		result.append("<p>maxLineCount=").append(maxLineCount).appendln("</p>");

		result.appendln("<p>");
		result.appendln("<table class=\"table_default\">");
		result.appendln("  <tr>");
		result.appendln("    <th>Nr.</th>");
		result.appendln("    <th>Word</th>");
		result.appendln("    <th>Length</th>");
		result.appendln("    <th>Count</th>");
		result.appendln("  </tr>");

		int lineCount = 0;
		for (StringLongPair pair : data)
		{
			lineCount += 1;

			result.appendln("  <tr>");
			result.append("    <td class=\"center\">").append(lineCount).appendln("</td>");
			result.append("    <td class=\"left\">").append(pair.getString()).appendln("</td>");
			result.append("    <td class=\"center\">").append(pair.getLong()).appendln("</td>");
			result.append("    <td class=\"center\">").append(pair.getCount()).appendln("</td>");
			result.appendln("  </tr>");
		}
		result.appendln("</table>");
		result.appendln("</p>");

		//
		return result;
	}

	/**
	 * Report word matching.
	 * 
	 * @param maxLineCount
	 *            the max line count
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList reportWordMatching(final int maxLineCount) throws IOException
	{
		StringList result;

		StringLongPairs data = StatisticsWorker.censusWordMatching(maxLineCount);

		result = new StringList();

		result.append("<H1>WORD MATCHING</H1>");
		result.append("<p>maxLineCount=").append(maxLineCount).appendln("</p>");

		result.appendln("<p>");
		result.appendln("<table class=\"table_default\">");
		result.appendln("  <tr>");
		result.appendln("    <th>Nr.</th>");
		result.appendln("    <th>Word</th>");
		result.appendln("    <th>Matching</th>");
		result.appendln("  </tr>");

		int lineCount = 0;
		for (StringLongPair pair : data)
		{
			lineCount += 1;

			result.appendln("  <tr>");
			result.append("    <td class=\"center\">").append(lineCount).appendln("</td>");
			result.append("    <td class=\"left\">").append(pair.getString()).appendln("</td>");
			result.append("    <td class=\"center\">").append(pair.getLong()).appendln("</td>");
			result.appendln("  </tr>");
		}
		result.appendln("</table>");
		result.appendln("</p>");

		//
		return result;
	}
}
