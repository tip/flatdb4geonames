/*
 * Copyright (C) 2015-2017 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.util.Comparator;

/**
 * The Class SingleLineBufferedFileReaderComparator.
 */
public class SingleLineBufferedFileReaderComparator implements Comparator<SingleLineBufferedFileReader>
{
	private Comparator<String> comparator;

	/**
	 * Instantiates a new single line buffered file reader comparator.
	 */
	public SingleLineBufferedFileReaderComparator()
	{
		this(null);
	}

	/**
	 * Instantiates a new single line buffered file reader comparator.
	 * 
	 * @param comparator
	 *            the comparator
	 */
	public SingleLineBufferedFileReaderComparator(final Comparator<String> comparator)
	{
		this.comparator = comparator;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final SingleLineBufferedFileReader alpha, final SingleLineBufferedFileReader bravo)
	{
		int result;

		//
		String alphaLine;
		if (alpha == null)
		{
			alphaLine = null;
		}
		else
		{
			alphaLine = alpha.getLine();
		}

		//
		String bravoLine;
		if (bravo == null)
		{
			bravoLine = null;
		}
		else
		{
			bravoLine = bravo.getLine();
		}

		//
		result = compare(alphaLine, bravoLine, this.comparator);

		//
		return result;
	}

	/**
	 * Compare.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 * @return the int
	 */
	public static int compare(final String alpha, final String bravo)
	{
		int result;

		result = compare(alpha, bravo, null);

		//
		return result;
	}

	/**
	 * This method compares two nullable string values.
	 * 
	 * Null values are sorted at the end of list to satisfied to shrink
	 * algorithm.
	 * 
	 * <pre>
	 * {@code 
	 * compare(null, null)   = 0
	 * compare(null, bravo)  > 0
	 * compare(alpha, null)  < 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * }
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * @param bravo
	 *            the other value.
	 * @param comparator
	 *            the comparator
	 * @return zero or a positive value or a negative value.
	 */
	public static int compare(final String alpha, final String bravo, final Comparator<String> comparator)
	{
		int result;

		//
		if ((alpha == null) && (bravo == null))
		{
			result = 0;
		}
		else if (alpha == null)
		{
			result = +1;
		}
		else if (bravo == null)
		{
			result = -1;
		}
		else
		{
			if (comparator == null)
			{
				result = alpha.compareTo(bravo);
			}
			else
			{
				result = comparator.compare(alpha, bravo);
			}
		}

		//
		return result;
	}
}
