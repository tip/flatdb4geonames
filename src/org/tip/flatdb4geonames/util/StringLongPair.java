/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

/**
 * The Class StringLongPair.
 */
public class StringLongPair
{
	private String aString;
	private Long aLong;
	private int count;

	/**
	 * Instantiates a new string long pair.
	 */
	public StringLongPair()
	{
		super();
	}

	/**
	 * Instantiates a new string long pair.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 */
	public StringLongPair(final String alpha, final int bravo)
	{
		super();

		this.aString = alpha;
		this.aLong = Long.valueOf(bravo);
	}

	/**
	 * Instantiates a new string long pair.
	 * 
	 * @param alpha
	 *            the alpha
	 * @param bravo
	 *            the bravo
	 */
	public StringLongPair(final String alpha, final Long bravo)
	{
		super();

		this.aString = alpha;
		this.aLong = bravo;
	}

	/**
	 * Gets the count.
	 * 
	 * @return the count
	 */
	public int getCount()
	{
		return this.count;
	}

	/**
	 * Gets the long.
	 * 
	 * @return the long
	 */
	public Long getLong()
	{
		Long result;

		result = this.aLong;

		//
		return result;
	}

	/**
	 * Gets the string.
	 * 
	 * @return the string
	 */
	public String getString()
	{
		String result;

		result = this.aString;

		//
		return result;
	}

	/**
	 * Inc.
	 */
	public void inc()
	{
		this.count += 1;
	}

	/**
	 * Sets the count.
	 * 
	 * @param count
	 *            the new count
	 */
	public void setCount(final int count)
	{
		this.count = count;
	}
}
