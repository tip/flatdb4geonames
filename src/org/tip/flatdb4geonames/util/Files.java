/*
 * Copyright (C) 2015-2024 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of FlatDB4GeoNames.
 *
 * FlatDB4GeoNames is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tip.flatdb4geonames.util;

import java.io.File;
import java.util.ArrayList;

/**
 * The Class Files.
 */
public class Files extends ArrayList<File>
{
	private static final long serialVersionUID = 6965074344170190507L;

	/**
	 * Instantiates a new files.
	 */
	public Files()
	{
		super();
	}

	/**
	 * Instantiates a new files.
	 * 
	 * @param capacity
	 *            the capacity
	 */
	public Files(final int capacity)
	{
		super(capacity);
	}
}
